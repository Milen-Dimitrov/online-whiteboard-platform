export class JWTPayload {
    id: number;
    email: string;
    displayName: string;
}
