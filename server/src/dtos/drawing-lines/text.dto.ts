import {  IsBoolean, IsNumber, IsString } from "class-validator";

export class TextDTO {

    @IsString()
    align: string;
    
    @IsString()
    fill: string;

    @IsNumber()
    fontSize: number;

    @IsString()
    fontStyle: string;

    @IsBoolean()
    textEditVisible: boolean;
    
    @IsString()
    textValue: string;
    
    @IsNumber()
    textX: number;

    @IsNumber()
    textY: number;
    
    @IsNumber()
    width: number;
}