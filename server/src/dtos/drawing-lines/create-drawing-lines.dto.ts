import {  IsNotEmpty, IsString } from "class-validator";
import { Point } from "src/models/point.entity";

export class CreateDrawingLineDTO {

    @IsString()
    @IsNotEmpty()
    tool: string;

    @IsString()
    @IsNotEmpty()
    color: string;

    points: number[];

}