import { Draw } from "src/models/draw.entity";

export class ReturnTextDTO {

id: number;
align: string;
fill: string;
fontSize: number;
fontStyle: string;
textEditVisible: boolean;
textValue: string;
textX: number;
textY: number;
width: number;
}