import { PointDTO } from "../points/point.dto";

export class ReturnDrawingLineDTO {
    id: number;
    tool: string;
    color: string;
    points: string;
}
