import { IsBoolean, IsNotEmpty, IsOptional, IsString, MaxLength } from "class-validator";

export class CreateKanbanDTO {

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    name: string;

    @IsOptional()
    @IsBoolean()
    isPublic: boolean;

}