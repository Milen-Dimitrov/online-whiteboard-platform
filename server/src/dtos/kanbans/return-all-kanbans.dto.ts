import { ReturnUserWithoutDTO } from "../users/user-without-relations.dto";

export class ReturnAllKanbanDTO {
  id: number;
  name: string;
  isPublic: boolean;
  kanbanCreator: ReturnUserWithoutDTO;
}
