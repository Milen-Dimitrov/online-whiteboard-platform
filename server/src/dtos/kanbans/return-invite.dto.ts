import { Kanban } from "src/models/kanban.entity";
import { User } from "src/models/user.entity";

export class ReturnInviteDTO {

id: number;

date: string;

invitedUsers: User;

kanbanTeamMembers: Kanban;

}