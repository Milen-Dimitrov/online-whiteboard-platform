import { ReturnLaneDTO } from "../lanes/return-lane.dto";
import { ReturnUserWithoutDTO } from "../users/user-without-relations.dto";

export class ReturnKanbanDTO {
  id: string;
  name: string;
  isPublic: boolean;
  kanbanCreator: ReturnUserWithoutDTO;
  lanes: ReturnLaneDTO[];
}
