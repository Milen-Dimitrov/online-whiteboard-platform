import { ReturnLaneDTO } from "../lanes/return-lane.dto";
import { ReturnUserDTO } from "../users/return-user.dto";
import { IsBoolean, IsNotEmpty, IsOptional, IsString, MaxLength } from "class-validator";

export class UpdateKanbanDTO {

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    name?: string;

    @IsBoolean()
    isPublic?: boolean;
}
