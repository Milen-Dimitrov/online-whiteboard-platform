
import {  IsNotEmpty, IsOptional, IsString, Length, MaxLength } from "class-validator";

export class CreateInviteDTO {

    @IsString()
    @Length(1, 200)
    invitationEmail: string;

}
