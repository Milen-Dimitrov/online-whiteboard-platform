import {  IsString, MaxLength } from "class-validator";

export class UpdateLaneDTO {

    @IsString()
    @MaxLength(15)
    title?: string;

}
