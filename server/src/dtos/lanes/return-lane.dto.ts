import { ReturnCardDTO } from '../cards/return-card.dto';

export class ReturnLaneDTO {
    id: string;
    title: string;
    isDeleted: boolean;
    cards: ReturnCardDTO[];
}
