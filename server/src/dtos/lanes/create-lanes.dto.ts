import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateLanesDTO {
  @IsString()
  @MaxLength(30)
  title: string;

  kanbanId: number;
}
