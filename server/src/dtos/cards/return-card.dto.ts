export class ReturnCardDTO {
    id: string;
    title: string;
    description: string;
    label: string;
}
