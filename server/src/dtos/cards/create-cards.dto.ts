import { IsString, MaxLength } from "class-validator";

export class CreateCardsDTO {

    @IsString()
    @MaxLength(30)
    title: string;

    @IsString()
    @MaxLength(70)
    description: string;
    
    
    label?: string;
}