export class ReturnUserWithoutDTO {
    id: number;
    email: string;
    displayName: string;
    picture: string;
  }