import { ReturnKanbanDTO } from "../kanbans/return-kanban.dto";

export class UserDTO {
    id: number;
    email: string;
    password: string;
    displayName: string;
    picture: string;
    kanbans: ReturnKanbanDTO[];
}
