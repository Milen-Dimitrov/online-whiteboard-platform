export class RecoverPassDTO {
  email: string;
  secretAnswer: string;
}
