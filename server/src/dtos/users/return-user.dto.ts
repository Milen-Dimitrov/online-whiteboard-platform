import { ReturnKanbanDTO } from "../kanbans/return-kanban.dto";

export class ReturnUserDTO {
  id: number;
  email: string;
  displayName: string;
  picture: string;
  kanbans: ReturnKanbanDTO[]
}
