import { Length, IsString, IsEmail } from 'class-validator';

export class CreateUserDTO {
  @IsEmail()
  @Length(4, 30)
  email: string;

  @IsString()
  @Length(4, 20)
  password: string;

  @IsString()
  @Length(4, 20)
  confirmPassword: string;

  @IsString()
  @Length(4, 30)
  displayName: string;

  @IsString()
  @Length(2, 20)
  secretAnswer: string;
}
