import { IsString, Length } from 'class-validator';

export class UpdatePasswordDTO {
    @IsString()
    @Length(4, 20)
    password: string;

    @IsString()
    @Length(4, 20)
    confirmPassword: string;
}