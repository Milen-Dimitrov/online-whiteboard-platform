import { IsString, Length } from 'class-validator';

export class UpdateUserDTO {
    @IsString()
    @Length(4, 20)
    displayName: string;
}
