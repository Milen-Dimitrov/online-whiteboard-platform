import { ReturnUserWithoutDTO } from "../users/user-without-relations.dto";
import { ReturnDrawingLineDTO } from "../drawing-lines/retunr-drawing-lines.dto";

export class ReturnDrawDTO {
    id: string;
    name: string;
    isPublic: boolean;
    drawCreator: ReturnUserWithoutDTO;
    lines: ReturnDrawingLineDTO[];
}
