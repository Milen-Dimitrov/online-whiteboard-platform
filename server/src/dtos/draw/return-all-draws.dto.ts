import { ReturnUserWithoutDTO } from "../users/user-without-relations.dto";

export class ReturnAllDrawsDTO {
    id: number;
    name: string;
    isPublic: boolean;
    drawCreator: ReturnUserWithoutDTO;
}