import { IsBoolean, IsNotEmpty, IsString, MaxLength } from "class-validator";

export class UpdateDrawDTO {

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    name?: string;

    @IsBoolean()
    isPublic?: boolean;
}
