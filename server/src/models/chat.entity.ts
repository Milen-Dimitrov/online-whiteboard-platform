import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';


@Entity('chat')
export class Chat {

    @PrimaryGeneratedColumn('increment')
    id: number;
    
    @Column({nullable: false, default: ''})
    username: string;

    @Column({nullable: false})
    room: string;

    @Column({nullable: false, default: ''})
    userId: string;

}