import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { DrawingLine } from './drawing-line.entity';

@Entity('point')
export class Point {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar' })
  cordinates: number;

  @ManyToOne(
    () => DrawingLine,
    point => point.id,
  )
  point: DrawingLine;
}
