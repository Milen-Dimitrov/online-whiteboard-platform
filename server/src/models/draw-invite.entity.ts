import { Entity, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, Column } from 'typeorm';
import { Draw } from './draw.entity';
import { User } from './user.entity';

@Entity('inviteddraw')
export class DrawInvite {

    @PrimaryGeneratedColumn('increment')
    id: number;
    
    @Column({ type: 'nvarchar', nullable: false, length: 20 })
    invitationEmail: string;

    @CreateDateColumn()
    date: Date;

    @ManyToOne(() => User, user => user.drawMember)
    invitedUsers: User;

    @ManyToOne(() => Draw, draw => draw.invitedPeople)
    draw: Draw;

}
