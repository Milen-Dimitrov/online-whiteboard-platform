import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Invite } from './invite.entity';
import { Lanes } from './lanes.entity';
import { User } from './user.entity';

@Entity('kanbans')
export class Kanban {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', nullable: false, length: 30 })
  name: string;

  @Column({ nullable: false, default: false })
  isPublic: boolean;

  @Column({ default: false })
  isDeleted: boolean;

  @ManyToOne(
    () => User,
    user => user.id,
  )
  kanbanCreator: User;

  @OneToMany(
    () => Lanes,
    lane => lane.parentKanban,
  )
  lanes: Lanes[];

  @OneToMany(
    () => Invite,
    invite => invite.kanban
  )
  invitedPeople: Invite[];
}
