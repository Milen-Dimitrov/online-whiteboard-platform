import { Entity, Column, PrimaryGeneratedColumn,  ManyToOne } from 'typeorm';
import { Draw } from './draw.entity';
import { Point } from './point.entity';

@Entity('lines')
export class DrawingLine {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', default: 'pen' })
  tool: string;

  @Column({ type: 'nvarchar', default: 'black' })
  color: string;

  @Column({type: "text", default: ""})
  points: string;

  @ManyToOne(
    () => Draw,
    draw => draw.id,
  )
  drawParent: Draw;
}
