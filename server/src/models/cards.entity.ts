import { ReturnLaneDTO } from 'src/dtos/lanes/return-lane.dto';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Lanes } from './lanes.entity';

@Entity('cards')
export class Cards {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', default: 'input title', length: 30 })
  title: string;

  @Column({ type: 'nvarchar', default: 'input description', length: 70 })
  description: string;

  @Column({ type: 'nvarchar', default: null, length: 30 })
  label: string;

  @Column({ nullable: false, default: false })
  isDeleted: boolean;

  @ManyToOne(
    () => Lanes,
    lane => lane.id,
  )
  parentLane: Lanes;
}
