import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Draw } from './draw.entity';

@Entity('text')
export class DrawingText {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  align: string;

  @Column({ default: 'black' })
  fill: string;

  @Column({ default: 0 })
  fontSize: number;

  @Column({ default: 'normal' })
  fontStyle: string;

  @Column()
  textEditVisible: boolean;

  @Column({ type: 'text' })
  textValue: string;

  @Column()
  textX: number;

  @Column()
  textY: number;
  
  @Column()
  width: number;

  @ManyToOne(
    () => Draw,
    draw => draw.text
  )
  textParent: Draw;
}