import { Entity, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, Column } from 'typeorm';
import { Kanban } from './kanban.entity';
import { User } from './user.entity';

@Entity('invite')
export class Invite {

    @PrimaryGeneratedColumn('increment')
    id: number;
    
    @Column({ type: 'nvarchar', nullable: false, length: 20 })
    invitationEmail: string;

    @CreateDateColumn()
    date: Date;

    @ManyToOne(() => User, user => user.kanbanMember)
    invitedUsers: User;

    @ManyToOne(() => Kanban, kanban => kanban.invitedPeople)
    kanban: Kanban;

}
