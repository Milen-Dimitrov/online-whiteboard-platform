import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { DrawInvite } from './draw-invite.entity';
import { Draw } from './draw.entity';
import { Invite } from './invite.entity';
import { Kanban } from './kanban.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', nullable: false, unique: true, length: 20 })
  email: string;

  @Column({ type: 'nvarchar', nullable: false, length: 20 })
  displayName: string;

  @Column({ type: 'nvarchar', nullable: false, length: 20 })
  secretAnswer: string;

  @Column({ type: 'nvarchar', nullable: false })
  password: string;

  @Column({ type: 'nvarchar', nullable: false })
  confirmPassword: string;

  @Column({
    type: 'nvarchar',
    nullable: false,
    default:
      'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png',
  })
  picture: string;

  @OneToMany(
    () => Kanban,
    kanban => kanban.kanbanCreator,
  )
  kanbans: Kanban[];

  @OneToMany(
    () => Draw,
    draws => draws.drawCreator,
  )
  draws: Draw[];

  @OneToMany(
    () => Invite,
    invite => invite.invitedUsers,
  )
  kanbanMember: Invite[];

  @OneToMany(
    () => DrawInvite,
    invite => invite.invitedUsers,
  )
  drawMember: DrawInvite[];
}
