import { DrawingText } from './text.entity';
import { TextDTO } from '../dtos/drawing-lines/text.dto';
import { ReturnDrawingLineDTO } from 'src/dtos/drawing-lines/retunr-drawing-lines.dto';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import {  DrawingLine } from './drawing-line.entity';
import { User } from './user.entity';
import { DrawInvite } from './draw-invite.entity';

@Entity('draw')
export class Draw {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', nullable: false, length: 30 })
  name: string;

  @Column({ default: false })
  isPublic: boolean;

  @Column({ default: false })
  isDeleted: boolean;

  @ManyToOne(
    () => User,
    user => user.id,
  )
  drawCreator: User;

  @OneToMany(
    () => DrawingLine,
    line => line.drawParent,
  )
  lines: ReturnDrawingLineDTO[];

  @OneToMany(
    () => DrawingText,
    text => text.textParent,
  )
  text: TextDTO[];

  @OneToMany(
    () => DrawInvite,
    invite => invite.draw
  )
  invitedPeople: DrawInvite[];
}