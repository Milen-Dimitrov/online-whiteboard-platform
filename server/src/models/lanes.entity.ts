import { ReturnKanbanDTO } from 'src/dtos/kanbans/return-kanban.dto';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Cards } from './cards.entity';
import { Kanban } from './kanban.entity';

@Entity('lanes')
export class Lanes {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'nvarchar', default: 'title', length: 30 })
  title: string;

  @Column({ nullable: false, default: false })
  isDeleted: boolean;

  @ManyToOne(
    () => Kanban,
    kanban => kanban.id,
  )
  parentKanban: Kanban;

  @OneToMany(
    () => Cards,
    card => card.parentLane,
    { eager: true },
  )
  cards: Cards[];
}
