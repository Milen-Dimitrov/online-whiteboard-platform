import { ReturnLaneDTO } from 'src/dtos/lanes/return-lane.dto';
import {
  Controller,
  Post,
  Body,
  Param,
  Put,
  Delete,
  Get,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateLanesDTO } from 'src/dtos/lanes/create-lanes.dto';
import { LanesService } from 'src/services/lanes.service';
import { UpdateLaneDTO } from 'src/dtos/lanes/update-lane.dto';

@Controller('lanes')
export class LanesController {
  public constructor(private readonly laneService: LanesService) {}

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async getLaneById(@Param('id') laneId: string): Promise<ReturnLaneDTO> {
    const lane = await this.laneService.getSingleLane(+laneId);

    return lane;
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  async create(@Body() lane: CreateLanesDTO): Promise<ReturnLaneDTO> {
    return await this.laneService.create(lane);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  async update(
    @Body() lane: UpdateLaneDTO,
    @Param('id') laneId: string,
  ): Promise<ReturnLaneDTO> {
    return await this.laneService.update(lane, +laneId);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async delete(@Param('id') laneId: string): Promise<{ message: string }> {
    return await this.laneService.delete(+laneId);
  }
}
