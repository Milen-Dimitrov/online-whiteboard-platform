import { Body, Controller, Param, Post, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { CreateInviteDTO } from "src/dtos/kanbans/create-invite-user.dto";
import { ReturnMessageDTO } from "src/dtos/return-message/return-message.dto";
import { DrawingInviteService } from "src/services/drawing-invite.service";


@Controller('invite')
export class DrawingInviteController {
    public constructor(private readonly drawingInvitesService: DrawingInviteService) { }
  
   
    @Post('draws/:drawId')
    @UseGuards(AuthGuard('jwt'))
    async createDrawInvite(@Body() invitationEmail: CreateInviteDTO, @Param('drawId') drawId: string): Promise<ReturnMessageDTO> {
  
      return await this.drawingInvitesService.drawInvite(invitationEmail, +drawId);
    }
}