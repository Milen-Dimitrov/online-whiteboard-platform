import { AuthController } from './auth.controllet';
import { UsersController } from './users.controller';
import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/services.module';
import { KanbansController } from './kanban.controller';
import { LanesController } from './lanes.controller';
import { CardsController } from './cards.controller';
import { InviteController } from './invite.controller';
import { DrawsController } from './draw.controller';
import { DrawingLineController } from './drawing-line.controller';
import { DrawingInviteController } from './drawing-invite.controller';

@Module({
    imports: [ ServicesModule ],
    controllers: [ UsersController, KanbansController, LanesController, CardsController, DrawsController, DrawingLineController,  InviteController, DrawingInviteController,   AuthController ]
})
export class ControllersModule { }
