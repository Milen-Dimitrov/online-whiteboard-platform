import { ReturnCardDTO } from './../dtos/cards/return-card.dto';
import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Put,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserId } from 'src/auth/user-id.decorator';
import { CreateKanbanDTO } from 'src/dtos/kanbans/create-kanban.dto';
import { ReturnAllKanbanDTO } from 'src/dtos/kanbans/return-all-kanbans.dto';
import { ReturnKanbanDTO } from 'src/dtos/kanbans/return-kanban.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { KanbansService } from 'src/services/kanban.service';
import { UpdateKanbanDTO } from 'src/dtos/kanbans/update-kanban-dto';
import { CardsService } from 'src/services/cards.service';
import { CreateCardsDTO } from 'src/dtos/cards/create-cards.dto';
import { UpdateCardDTO } from 'src/dtos/cards/update-card.dto';

@Controller('lanes')
export class CardsController {
  public constructor(private readonly cardsService: CardsService) {}

  @Post(':laneId/cards')
  @UseGuards(AuthGuard('jwt'))
  async create(
    @Body() card: Partial<CreateCardsDTO>,
    @Param('laneId') laneId: string,
  ): Promise<ReturnCardDTO> {
    return await this.cardsService.create(card, +laneId);
  }

  @Put('/cards/:id')
  @UseGuards(AuthGuard('jwt'))
  async updateCard(
    @Param('id') cardId: string,
    @Body() card: UpdateCardDTO,
  ): Promise<ReturnMessageDTO> {
    return await this.cardsService.update(card, +cardId);
  }

  @Delete('/cards/:id')
  @UseGuards(AuthGuard('jwt'))
  async deleteCard(@Param('id') cardId: string): Promise<ReturnMessageDTO> {
    return await this.cardsService.delete(+cardId);
  }
}
