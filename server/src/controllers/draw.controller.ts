import { Controller, Post, Body, Get, Param, Put, Delete, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserId } from 'src/auth/user-id.decorator';
import { CreateDrawDTO } from 'src/dtos/draw/create-draw.dto';
import { ReturnAllDrawsDTO } from 'src/dtos/draw/return-all-draws.dto';
import { ReturnDrawDTO } from 'src/dtos/draw/return-draw.dto';
import { UpdateDrawDTO } from 'src/dtos/draw/update-draw.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { DrawsService } from 'src/services/draw.service';


@Controller('draws')
export class DrawsController {

    public constructor(
        private readonly drawsService: DrawsService,
    ) { }



    @Get()
    @UseGuards(AuthGuard('jwt'))
    async allDraws(): Promise<ReturnAllDrawsDTO[]> {
        const draws = await this.drawsService.getAll();
        return draws;
    }

    @Get('/private')
    @UseGuards(AuthGuard('jwt'))
    async allPrivateDraws(@UserId() userId: number): Promise<ReturnAllDrawsDTO[]> {
    const draws = await this.drawsService.getAllPrivate(userId);
    return draws;
  }
  @Get('/public')
    @UseGuards(AuthGuard('jwt'))
    async getAllPublicDraws(@UserId() userId: number): Promise<ReturnAllDrawsDTO[]> {
    const draws = await this.drawsService.getAllPublic(userId);
    return draws;
  }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'))
    async oneDraw(@Param('id') drawId: number, @UserId() userId: number): Promise<ReturnDrawDTO> {
        const draw = await this.drawsService.getSingleDraw(drawId, userId);

        return draw;
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    async create(@Body() draw: CreateDrawDTO, @UserId() userId: number): Promise<ReturnDrawDTO> {

        return await this.drawsService.create(draw, userId);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    async updateDraw(@Param('id') drawId: string, @Body() drawDTO: Partial<UpdateDrawDTO>): Promise<Partial<UpdateDrawDTO>> {

        return await this.drawsService.updateDrawDetails(drawDTO, +drawId);
    }


    @Delete(':Id')
    @UseGuards(AuthGuard('jwt'))
    async deleteDraw(@Param('Id') drawId: string, @UserId() UserId: number): Promise<ReturnMessageDTO> {

        return await this.drawsService.delete(+drawId, UserId);
    }

}
