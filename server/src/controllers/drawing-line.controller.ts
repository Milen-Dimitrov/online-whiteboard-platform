import { TextDTO } from '../dtos/drawing-lines/text.dto';
import { ReturnLaneDTO } from 'src/dtos/lanes/return-lane.dto';
import {
  Controller,
  Post,
  Body,
  Param,
  Put,
  Delete,
  Get,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';

import { CreateDrawingLineDTO } from 'src/dtos/drawing-lines/create-drawing-lines.dto';
import { ReturnDrawingLineDTO } from 'src/dtos/drawing-lines/retunr-drawing-lines.dto';
import { DrawingLinesService } from 'src/services/drawing-lines.service';

@Controller('draws')
export class DrawingLineController {
  public constructor(private readonly lineService: DrawingLinesService) {}

  @Post(':id/lines')
  @UseGuards(AuthGuard('jwt'))
  async create(@Body() line: CreateDrawingLineDTO, @Param('id') drawId: string): Promise<ReturnDrawingLineDTO> {
    return await this.lineService.create(line, +drawId);
  }

  @Post(':id/text')
  @UseGuards(AuthGuard('jwt'))
  async createText(@Body() text: TextDTO, @Param('id') drawId: string): Promise<TextDTO> {
    
    return await this.lineService.textCreate(text, +drawId);
  }

}
