import { Controller, Post, Body, ValidationPipe, Delete, Put } from '@nestjs/common';
import { AuthService } from 'src/services/auth.service';
import { UserLoginDTO } from 'src/dtos/users/user-login.dto';
import { GetToken } from 'src/auth/get-token.decorator';
import { RecoverPassDTO } from 'src/dtos/users/recover-pass.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';

@Controller('session')
export class AuthController {

    constructor(
        private readonly authService: AuthService,
    ) { }

    @Post()
    async login(@Body(new ValidationPipe({ whitelist: true })) userDto: UserLoginDTO): Promise<{ token: string }> {
        return await this.authService.login(userDto.email, userDto.password);
    } 

    @Delete()
    async logout(@GetToken() token: string): Promise<{ message: string }> {
        await this.authService.blacklist(token?.slice(7));

        return {
            message: 'You have been logged out!',
        };
    }



    @Put()
    async lostPassword(@Body() userdto: RecoverPassDTO ):  Promise<{ token: string }> {
        
        return await this.authService.forgotPassword(userdto);
    }

}
