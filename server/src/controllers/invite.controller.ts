import { Body, Controller, Param, Post, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { CreateInviteDTO } from "src/dtos/kanbans/create-invite-user.dto";
import { ReturnMessageDTO } from "src/dtos/return-message/return-message.dto";
import { InviteService } from "src/services/invite.service";


@Controller('invite')
export class InviteController {
    public constructor(private readonly invitesService: InviteService) { }
  
    @Post('kanbans/:kanbanId')
    @UseGuards(AuthGuard('jwt'))
    async create(@Body() invitationEmail: CreateInviteDTO, @Param('kanbanId') kanbanId: string): Promise<ReturnMessageDTO> {
  
      return await this.invitesService.create(invitationEmail, +kanbanId);
    }

   
}