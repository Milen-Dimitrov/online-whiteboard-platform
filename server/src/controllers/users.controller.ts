import { Controller, Post, Body, HttpCode, HttpStatus, Put, UseGuards, Param, Get } from '@nestjs/common';
import { CreateUserDTO } from 'src/dtos/users/create-user.dto';
import { UsersService } from 'src/services/user.service';
import { UpdateUserDTO } from 'src/dtos/users/update-user.dto';
import { UserId } from 'src/auth/user-id.decorator';
import { AuthGuard } from '@nestjs/passport';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { UpdatePasswordDTO } from 'src/dtos/users/update-password.dto';

@Controller('users')
export class UsersController {

  public constructor(private readonly usersService: UsersService) { }


  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async getById(@Param('id') userID: string): Promise<ReturnUserDTO> {
    const user = await this.usersService.getById(+userID);
    return user;
  }

  @Post()
  public async addNewUser(@Body() user: CreateUserDTO): Promise<ReturnMessageDTO> {

    return this.usersService.create(user);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  async updateUser(@Body() userdto: UpdateUserDTO, @UserId() loggedUser: number): Promise<ReturnUserDTO> {

    return await this.usersService.update(userdto, loggedUser);
  }

  @Put(':id/pass')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  async updateUserPass(@Body() userdto: UpdatePasswordDTO, @UserId() loggedUser: number): Promise<ReturnMessageDTO> {

    return await this.usersService.updatePass(userdto, loggedUser);
  }
}
