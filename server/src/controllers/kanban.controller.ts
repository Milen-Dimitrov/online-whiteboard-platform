import { Controller, Post, Body, Get, Param, Put, Delete, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserId } from 'src/auth/user-id.decorator';
import { CreateKanbanDTO } from 'src/dtos/kanbans/create-kanban.dto';
import { ReturnAllKanbanDTO } from 'src/dtos/kanbans/return-all-kanbans.dto';
import { ReturnKanbanDTO } from 'src/dtos/kanbans/return-kanban.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { KanbansService } from 'src/services/kanban.service';
import { UpdateKanbanDTO } from 'src/dtos/kanbans/update-kanban-dto';

@Controller('kanbans')
export class KanbansController {
    public constructor(
        private readonly kanbansService: KanbansService,
    ) { }


  @Get()
    @UseGuards(AuthGuard('jwt'))
    async allKanbans(): Promise<ReturnAllKanbanDTO[]> {
    const kanbans = await this.kanbansService.getAll();
    return kanbans;
  }

  @Get('/private')
    @UseGuards(AuthGuard('jwt'))
    async allPrivateKanbans(@UserId() userId: number): Promise<ReturnAllKanbanDTO[]> {
    const kanbans = await this.kanbansService.getAllPrivate(userId);
    return kanbans;
  }
  @Get('/public')
    @UseGuards(AuthGuard('jwt'))
    async getAllPublic(@UserId() userId: number): Promise<ReturnAllKanbanDTO[]> {
    const kanbans = await this.kanbansService.getAllPublic(userId);
    return kanbans;
  }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'))
    async oneKanban(@Param('id') kanbanId: number, @UserId() userId: number): Promise<ReturnKanbanDTO> {
    const kanban = await this.kanbansService.getSingleKanban(kanbanId, userId);

    return kanban;
    }
  
    @Post()
    @UseGuards(AuthGuard('jwt'))
    async create(@Body() kanban: CreateKanbanDTO, @UserId() userId: number): Promise<ReturnKanbanDTO> {
        return await this.kanbansService.create(kanban, userId);
    }
    
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    async updateKanban(@Param('id') kanbanId: string, @Body() kanbanDTO: Partial<UpdateKanbanDTO>): Promise<Partial<UpdateKanbanDTO>> {

        return await this.kanbansService.updateKanbanDetails(kanbanDTO, +kanbanId);
    }


    @Delete(':Id')
    @UseGuards(AuthGuard('jwt'))
    async deleteKanban(@Param('Id') kanbanId: string, @UserId() UserId: number): Promise<ReturnMessageDTO> {

        return await this.kanbansService.delete(+kanbanId, UserId);
    }

}
