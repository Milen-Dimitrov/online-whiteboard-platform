import { DrawingText } from './../models/text.entity';
import { TransformService } from './transformer.service';
import { User } from 'src/models/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { UsersService } from './user.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './../constant/secret';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt-strategy';
import { Token } from 'src/models/token.entity';
import { KanbansService } from './kanban.service';
import { Kanban } from 'src/models/kanban.entity';
import { Lanes } from 'src/models/lanes.entity';
import { Cards } from 'src/models/cards.entity';
import { LanesService } from './lanes.service';
import { CardsService } from './cards.service';
import { Invite } from 'src/models/invite.entity';
import { InviteService } from './invite.service';
import { ChatGateway } from './chatt.gateway'
import { ChatService } from './chat.service'
import { Chat } from 'src/models/chat.entity';
import { Draw } from 'src/models/draw.entity';
import { DrawsService } from './draw.service';
import { DrawingLine } from 'src/models/drawing-line.entity';
import { DrawingLinesService } from './drawing-lines.service';
import { DrawInvite } from 'src/models/draw-invite.entity';
import { DrawingInviteService } from './drawing-invite.service';

@Module({
  imports: [

    TypeOrmModule.forFeature([User, Kanban, Token, Lanes, Cards, Invite, DrawInvite, Chat, Draw, DrawingLine, DrawingText]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d',
      }
    })
  ],
  providers: [UsersService, KanbansService, LanesService, CardsService, TransformService, InviteService, DrawingInviteService, DrawsService, DrawingLinesService,  AuthService, JwtStrategy, ChatGateway, ChatService],
  exports: [UsersService, KanbansService, LanesService, CardsService, TransformService, InviteService, DrawingInviteService, DrawsService, DrawingLinesService,  AuthService, ChatGateway, ChatService]
})
export class ServicesModule { }
