import { ReturnCardDTO } from 'src/dtos/cards/return-card.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { TransformService } from './transformer.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { Lanes } from 'src/models/lanes.entity';
import { Kanban } from 'src/models/kanban.entity';
import { Cards } from 'src/models/cards.entity';
import { CreateCardsDTO } from 'src/dtos/cards/create-cards.dto';
import { UpdateCardDTO } from 'src/dtos/cards/update-card.dto';

@Injectable()
export class CardsService {
  constructor(
    private readonly transformer: TransformService,
    @InjectRepository(Lanes)
    private readonly lanesRepository: Repository<Lanes>,
    @InjectRepository(Kanban)
    private readonly kanbansRepository: Repository<Kanban>,
    @InjectRepository(Cards)
    private readonly cardsRepository: Repository<Cards>,
  ) {}

  async create(
    card: Partial<CreateCardsDTO>,
    laneId: number,
  ): Promise<ReturnCardDTO> {
    const laneFound = await this.findOneOrFailLane(laneId);
    const createCard = this.cardsRepository.create(card);

    createCard.parentLane = laneFound;

    const createdCard = await this.cardsRepository.save(createCard);
    return this.transformer.toReturnCardDTO(createdCard);
  }

  async update(card: UpdateCardDTO, cardId: number): Promise<ReturnMessageDTO> {
    const cardFound = await this.cardsRepository.findOne({
      where: {
        id: cardId,
        isDeleted: false,
      },
      relations: ['parentLane'],
    });

    if (!cardFound) {
      throw new NotFoundException('Card with this ID does not exist');
    }

    cardFound.parentLane.id = card.toLaneId;

    await this.cardsRepository.update(cardId, cardFound);

    return { message: 'Card updated!' };
  }

  async delete(cardId: number): Promise<ReturnMessageDTO> {
    const cardFound = await this.cardsRepository.findOne(cardId);

    if (!cardFound) {
      throw new NotFoundException('Card with this ID does not exist');
    }

    cardFound.isDeleted = true;
    await this.cardsRepository.save(cardFound);

    return { message: `Card with id ${cardFound.id} is deleted!` };
  }

  private async findOneOrFailLane(laneId: number): Promise<Lanes> {
    const lane = await this.lanesRepository.findOne(laneId, {
      relations: ['cards'],
    });

    if (!lane) {
      throw new NotFoundException('No Lane found!');
    }

    return lane;
  }
}
