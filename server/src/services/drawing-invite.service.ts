import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { TransformService } from './transformer.service';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { CreateInviteDTO } from 'src/dtos/kanbans/create-invite-user.dto';
import { Draw } from 'src/models/draw.entity';
import { DrawInvite } from 'src/models/draw-invite.entity';


@Injectable()
export class DrawingInviteService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(DrawInvite) private readonly drawingInvitesRepository: Repository<DrawInvite>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Draw) private readonly drawsRepository: Repository<Draw>,

    ) { }


async drawInvite(email: CreateInviteDTO, drawId: number, ): Promise<ReturnMessageDTO> {

    const userFound = await this.usersRepository.findOne({
        where : {
          email: email.invitationEmail
        },
        relations: ['drawMember']
    });
    const drawFound = await this.drawsRepository.findOne({
        where: {
            id: drawId,
        },
    });

    if (email.invitationEmail === null || email.invitationEmail === undefined || email.invitationEmail === '') {
        throw new BadRequestException('Email can\'t be an empty field');
    }

    const createInvite = this.drawingInvitesRepository.create(email);
    createInvite.invitedUsers = userFound
    createInvite.draw = drawFound
    
    await this.drawingInvitesRepository.save(createInvite);
    await this.drawsRepository.update(drawId, drawFound);

    return { message: 'User invited!' };

}

}