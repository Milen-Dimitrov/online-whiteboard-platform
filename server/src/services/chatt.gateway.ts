import { SubscribeMessage, 
    WebSocketGateway, 
    OnGatewayInit, 
    WebSocketServer, 
    OnGatewayConnection, 
    OnGatewayDisconnect } from '@nestjs/websockets';
import { Logger, Inject } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { ChatService } from './chat.service'


@WebSocketGateway ()
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect  {

    
    @Inject()
    chatService: ChatService

    @WebSocketServer()
    server: Server;

    private logger: Logger = new Logger ('ChatGateway')

    @SubscribeMessage('msgToServer')
    async handleMessage (client: Socket, message: {sender: string, room: string, message: string}): Promise<void> {
        this.server.to(message.room).emit('msgToClient', message)
        this.logger.log(`msgToClient: ${message}` )
    }

    @SubscribeMessage('joinRoom')
    async handleRoomJoin ( client: Socket, userRoom: { userId: string, userName: string, room: string } ): Promise<void> {

        await this.chatService.userJoin(userRoom)
        client.join(userRoom.room)
        const partisipants = await this.chatService.getRoomUsers(userRoom.room);
        
        this.server.to(userRoom.room).emit('joinedRoom', partisipants);
        this.logger.log(`Client joined ${userRoom.room}`)
    }

    async afterInit (server: Server): Promise<void> {
        this.logger.log('init');
    }

    async handleDisconnect (client: Socket): Promise<void> {
        const userRoomList = await this.chatService.userClose(client.id);
        const iterate = async (item) => {
            await this.chatService.userLeave(item.userId, item.room)

            client.leave(item.room)
            const partisipants = await this.chatService.getRoomUsers(item.room);
            this.server.to(item.room).emit('leftRoom', partisipants)
            client.emit('leftRoom', partisipants);
        }

        userRoomList.forEach(iterate)

        this.logger.log(`Client disconnected: ${client.id}`)
        }
        
    async handleConnection (client: Socket, ...args: any[] ): Promise<void> {

        this.logger.log(`Client connected ${client.id}`)
    }

    @SubscribeMessage('leaveRoom')
    async handleRoomLeave (client: Socket, userRoom: {userId: string, userName: string, room: string}): Promise<void> {

        await this.chatService.userLeave(userRoom.userId, userRoom.room);
        client.leave(userRoom.room);
        const partisipants = await this.chatService.getRoomUsers(userRoom.room);
        this.server.to(userRoom.room).emit('leftRoom', partisipants)
        client.emit('leftRoom', partisipants);
        this.logger.log(`Client left ${userRoom.room}`)
    }
}
