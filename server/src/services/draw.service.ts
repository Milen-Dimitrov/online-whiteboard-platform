import { Lanes } from 'src/models/lanes.entity';
import {
    Injectable,
    BadRequestException,
    NotFoundException,
} from '@nestjs/common';
import { TransformService } from './transformer.service';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Draw } from 'src/models/draw.entity';
import { CreateDrawDTO } from 'src/dtos/draw/create-draw.dto';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { UpdateDrawDTO } from 'src/dtos/draw/update-draw.dto';

@Injectable()
export class DrawsService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Draw)
        private readonly drawsRepository: Repository<Draw>,
        @InjectRepository(Lanes)
        private readonly lanesRepository: Repository<Lanes>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
    ) {
        this.drawsRepository = drawsRepository;
        this.usersRepository = usersRepository;
        this.transformer = transformer;
    }

    async getAll() {
        const draws = await this.drawsRepository.find({
            where: {
                isDeleted: false,
                isPublic: true,
            },
            relations: ['drawCreator'],
        });
        return draws.map(draw => this.transformer.toReturnAllDrawsDTO(draw));
    }

    async getAllPrivate(userId: number) {
        const foundUser = await this.findOneOrFailUser(userId);
        const draws = await this.drawsRepository.find({
            where: {
                isDeleted: false,
                isPublic: false,
                drawCreator: foundUser,
            },
            relations: ['drawCreator'],
        });

        return draws.map(draw => this.transformer.toReturnAllDrawsDTO(draw));
    }

    async getAllPublic(userId: number) {
        const foundUser = await this.findOneOrFailUser(userId);
        const draws = await this.drawsRepository.find({
            where: {
                isDeleted: false,
                isPublic: true,
                drawCreator : foundUser,
            },
            relations: ['drawCreator'],
        });
       
        return draws.map(draw => this.transformer.toReturnAllDrawsDTO(draw));
    }

    async getSingleDraw(drawId: number, userId: number): Promise<any> {
        const userFound = await this.findOneOrFailUser(userId);
        const draw = await this.drawsRepository.findOne(drawId,{
            where: { 
                isDeleted: false,
            },
            relations: [ 'drawCreator', 'lines', 'text', 'invitedPeople'],
        });
        
        const person = draw.invitedPeople.find(el => el.invitationEmail === userFound.email)
        if (userFound.id !== draw.drawCreator.id     
            && draw.isPublic === false
            && (person === undefined)
            ) {
            throw new BadRequestException('You can not access another users private draws.')
        }

        return this.transformer.toReturnDrawDTO(draw)
    }

    async create(
        draw: CreateDrawDTO,
        userId: number,
    ): Promise<any> {
        const userFound = await this.findOneOrFailUserWithoutRelations(userId);

        if (
            draw.name === null ||
            draw.name === undefined ||
            draw.name === ''
        ) {
            throw new BadRequestException("Name can't be an empty field");
        }
        const createDraw = await this.drawsRepository.create(draw);
        createDraw.drawCreator = userFound;

        await this.drawsRepository.save(createDraw);

        return this.transformer.toReturnDrawDTO(createDraw);
    }


    async updateDrawDetails( draw: Partial<UpdateDrawDTO>, drawId: number ): Promise<Partial<UpdateDrawDTO>> {
        const drawToUpdate = await this.findOneOrFailDraw(drawId);
        if (draw.name === drawToUpdate.name) {
          throw new BadRequestException(
            'There is already a draw with this name. Names must be unique!',
          );
        }
        await this.drawsRepository.update(drawId, draw);
        
        return draw;
      }

    async delete(drawId: number, userId: number): Promise<ReturnMessageDTO> {
        const foundUser = await this.findOneOrFailUser(userId);
        const foundDraw = await this.drawsRepository.findOne(drawId);

        if (!foundDraw) {
            throw new NotFoundException(`There is no draw with id ${drawId}!`);
        }

        foundDraw.isDeleted = true;
        await this.drawsRepository.save(foundDraw);

        return { message: `Kanban with id ${foundDraw.id} is deleted!` };
    }



    private async findOneOrFailUser(userId: number): Promise<User> {
        const user = await this.usersRepository.findOne(userId, {
            relations: ['draws'],
        });

        if (!user) {
            throw new Error('No user!');
        }

        return user;
    }

    private async findOneOrFailUserWithoutRelations(userId: number): Promise<User> {
        const user = await this.usersRepository.findOne(userId);

        if (!user) {
            throw new Error('No user!');
        }

        return user;
    }

    private async findOneOrFailDraw(drawId: number): Promise<Draw> {
        const draw = await this.drawsRepository.findOne(drawId);
        if (!draw) {
            throw new NotFoundException('There is no such draw!');
        }

        return draw;
    }
}
