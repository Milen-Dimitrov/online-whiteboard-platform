import { Injectable, BadRequestException } from '@nestjs/common';
import { TransformService } from './transformer.service';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { Kanban } from 'src/models/kanban.entity';
import { Invite } from 'src/models/invite.entity';
import { CreateInviteDTO } from 'src/dtos/kanbans/create-invite-user.dto';
import { Draw } from 'src/models/draw.entity';


@Injectable()
export class InviteService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(Invite) private readonly invitesRepository: Repository<Invite>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Kanban) private readonly kanbansRepository: Repository<Kanban>,
        @InjectRepository(Draw) private readonly drawsRepository: Repository<Draw>,

    ) { }

async create(email: CreateInviteDTO, kanbanId: number, ): Promise<ReturnMessageDTO> {

    const userFound = await this.usersRepository.findOne({
        where : {
          email: email.invitationEmail
        },
        relations: ['kanbanMember']
    });
    const kanbanFound = await this.kanbansRepository.findOne({
        where: {
            id: kanbanId,
        },
    });

    if (email.invitationEmail === null || email.invitationEmail === undefined || email.invitationEmail === '') {
        throw new BadRequestException('Email can\'t be an empty field');
    }

    const createInvite = await this.invitesRepository.create(email);
    createInvite.invitedUsers = userFound
    createInvite.kanban = kanbanFound
    
    await this.invitesRepository.save(createInvite);
    await this.kanbansRepository.update(kanbanId, kanbanFound);

    return { message: 'User invited!' };

}

}