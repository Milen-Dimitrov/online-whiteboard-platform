import { DrawingText } from './../models/text.entity';
import { User } from 'src/models/user.entity';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { use } from 'passport';
import { Kanban } from 'src/models/kanban.entity';
import { ReturnKanbanDTO } from 'src/dtos/kanbans/return-kanban.dto';
import { ReturnAllKanbanDTO } from 'src/dtos/kanbans/return-all-kanbans.dto';
import { Lanes } from 'src/models/lanes.entity';
import { ReturnLaneDTO } from 'src/dtos/lanes/return-lane.dto';
import { Cards } from 'src/models/cards.entity';
import { ReturnCardDTO } from 'src/dtos/cards/return-card.dto';
import { ReturnAllDrawsDTO } from 'src/dtos/draw/return-all-draws.dto';
import { ReturnDrawDTO } from 'src/dtos/draw/return-draw.dto';
import { Draw } from 'src/models/draw.entity';
import { DrawingLine } from 'src/models/drawing-line.entity';
import { ReturnDrawingLineDTO } from 'src/dtos/drawing-lines/retunr-drawing-lines.dto';

export class TransformService {
  toReturnUserDTO(user: User): ReturnUserDTO {
    return {
      id: user.id,
      email: user.email,
      displayName: user.displayName,
      picture: user.picture,
      kanbans: user.kanbans
        ? user.kanbans?.map(kanban => this.toReturnKanbanDTO(kanban))
        : undefined,
    };
  }

  toReturnKanbanDTO(kanban: Kanban): ReturnKanbanDTO {
    return {
      id: String(kanban.id),
      name: kanban.name,
      isPublic: kanban.isPublic,
      kanbanCreator: kanban.kanbanCreator,
      lanes: kanban.lanes
        ? kanban.lanes
            .filter(lane => lane.isDeleted === false)
            .map(lane => ({
              ...lane,
              id: String(lane.id),
              cards:
                lane.cards?.map(card => ({ ...card, id: String(card.id) }))
                .filter(card => card.isDeleted === false) ||
                [],
            }))
        : [],
    };
  }

  toReturnAllKanbanDTO(kanban: Kanban): ReturnAllKanbanDTO {
    return kanban
      ? {
          id: kanban.id,
          name: kanban.name,
          isPublic: kanban.isPublic,
          kanbanCreator: kanban.kanbanCreator
            ? kanban.kanbanCreator
            : undefined,
        }
      : undefined;
  }

  toReturnLaneDTO(lane: Lanes): ReturnLaneDTO {
    return lane
      ? {
          id: String(lane.id),
          title: lane.title,
          isDeleted: lane.isDeleted,
          cards:
            lane.cards?.map(card => ({ ...card, id: String(card.id) })) || [],
        }
      : undefined;
  }

  toReturnCardDTO(card: Cards): ReturnCardDTO {
    return card
      ? {
          id: String(card.id),
          title: card.title,
          description: card.description,
          label: card.label ? card.label : undefined,
        }
      : undefined;
  }

  toReturnDrawDTO(draw: Draw) {
    return {
      id: String(draw.id),
      name: draw.name,
      isPublic: draw.isPublic,
      drawCreator: draw.drawCreator,
      lines: draw.lines
       ? draw.lines
       .map((line) => ({
        id: line.id,
        tool: line.tool,
        color: line.color,
        points: line.points.split(',').map(p => +p)
      }))
      : [],
      text: draw.text
    }
  }

  toReturnAllDrawsDTO(draw: Draw): ReturnAllDrawsDTO {
    return draw
      ? {
        id: draw.id,
        name: draw.name,
        isPublic: draw.isPublic,
        drawCreator: draw.drawCreator
          ? draw.drawCreator
          : undefined,
      }
      : undefined;
  }

  toReturnDrawingLineDTO(line: DrawingLine) {
    return line
      ? {
          id: line.id,
          tool: line.tool,
          color: line.color,
          points: line.points
        }
      : undefined;
  }

  toReturnTextDTO(text: DrawingText) {

    return text 
    ? {
      id: text.id,
      align: text.align,
      fill: text.fill,
      fontSize: text.fontSize,
      fontStyle: text.fontStyle,
      textEditVisible: text.textEditVisible,
      textValue: text.textValue,
      textX: text.textX,
      textY: text.textY,
      width: text.width,
    }
    : undefined
  }
}
