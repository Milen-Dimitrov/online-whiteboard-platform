import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Chat } from '../models/chat.entity';


@Injectable()
export class ChatService {

    constructor(
        @InjectRepository(Chat) 
        private readonly chatRepository: Repository<Chat>
    ) {}

    async userJoin (userRoom: { userId: string, userName: string, room: string}): Promise<void> {
        await this.chatRepository.save(userRoom)
    }

    async getCurrentUser (userId: string): Promise<any> {
        const user = this.chatRepository.findOne({
            where: {
                id: userId
            }
        })

        return user;
    }

    async userLeave (userId: string, room: string): Promise<void> {
        const user = this.chatRepository.findOne({
            where: {
            id: userId,
            room: room
            }
        })

    }

    async userClose(userId: string): Promise<any> {
        const userRooms = await this.chatRepository.find({
            where: {
                id: userId
            }
        })
        
        return userRooms;
    }

    async getRoomUsers (room: string): Promise<any> {
        const partisipants = await this.chatRepository.find({
            select: ['username', 'id'],
            where: {
                room: room
            }
        })

        return partisipants;
    }
}