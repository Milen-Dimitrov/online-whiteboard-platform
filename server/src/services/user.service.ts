import { User } from 'src/models/user.entity';
import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDTO } from 'src/dtos/users/create-user.dto';
import { UpdateUserDTO } from 'src/dtos/users/update-user.dto';
import * as bcrypt from 'bcrypt';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { ReturnUserDTO } from 'src/dtos/users/return-user.dto';
import { TransformService } from './transformer.service';
import { UpdatePasswordDTO } from 'src/dtos/users/update-password.dto';

@Injectable()
export class UsersService {
    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,

    ) { }


    async getById(userId: number): Promise<ReturnUserDTO> {
        const user = await this.usersRepository.findOne(
            {
                where: {
                    id: userId,
                },
                relations: ['kanbans']
            });
        if (!user) {
            throw new BadRequestException('No such user');
        }

        return this.transformer.toReturnUserDTO(user);
    }

    async create(userDTO: CreateUserDTO): Promise<ReturnMessageDTO> {

        const user = this.usersRepository.create(userDTO);
        user.password = await bcrypt.hash(user.password, 10);
        user.confirmPassword = await bcrypt.hash(user.confirmPassword, 10);
        await this.usersRepository.save(user);

        return { message: 'User created' };
    }


    async updatePass(userDTO: UpdatePasswordDTO, loggedUser: number): Promise<ReturnMessageDTO> {
        const user = await this.usersRepository.findOneOrFail(loggedUser);
        if (!user) {
            throw new NotFoundException('no such user')
        }

    
        user.password = await bcrypt.hash(userDTO.password, 10)
        user.confirmPassword = await bcrypt.hash(userDTO.confirmPassword, 10)
        await this.usersRepository.update(loggedUser, user);
       
        return { message: 'User updated' };
    }

    async update(userDTO: UpdateUserDTO, loggedUser: number): Promise<ReturnUserDTO> {
        const user = await this.usersRepository.findOneOrFail(loggedUser);
        if (!user) {
            throw new NotFoundException('no such user')
        }

        user.displayName = userDTO.displayName;
        await this.usersRepository.update(loggedUser, user);

        const updated = this.transformer.toReturnUserDTO(user);
        return updated;
    }

}
