import { Lanes } from 'src/models/lanes.entity';
import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { TransformService } from './transformer.service';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { Kanban } from 'src/models/kanban.entity';
import { CreateKanbanDTO } from 'src/dtos/kanbans/create-kanban.dto';
import { UpdateKanbanDTO } from 'src/dtos/kanbans/update-kanban-dto';
import { ReturnKanbanDTO } from 'src/dtos/kanbans/return-kanban.dto';

@Injectable()
export class KanbansService {
  constructor(
    private readonly transformer: TransformService,
    @InjectRepository(Kanban)
    private readonly kanbansRepository: Repository<Kanban>,
    @InjectRepository(Lanes)
    private readonly lanesRepository: Repository<Lanes>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {
    this.kanbansRepository = kanbansRepository;
    this.usersRepository = usersRepository;
    this.transformer = transformer;
  }

  async getAll() {
    const kanbans = await this.kanbansRepository.find({
      where: {
        isDeleted: false,
        isPublic: true,
      },
      relations: ['kanbanCreator'],
    });
    return kanbans.map(kanban => this.transformer.toReturnAllKanbanDTO(kanban));
  }

  async getAllPrivate(userId: number) {
    const foundUser = await this.findOneOrFailUser(userId);
    const kanbans = await this.kanbansRepository.find({
      where: {
        isDeleted: false,
        kanbanCreator: foundUser,
        isPublic: false,
      },
      relations: ['kanbanCreator'],
    });
    return kanbans.map(kanban => this.transformer.toReturnAllKanbanDTO(kanban));
  }

  async getAllPublic(userId: number) {
    const foundUser = await this.findOneOrFailUser(userId);
    const kanbans = await this.kanbansRepository.find({
      where: {
        isDeleted: false,
        kanbanCreator: foundUser,
        isPublic: true,
      },
      relations: ['kanbanCreator'],
    });
    return kanbans.map(kanban => this.transformer.toReturnAllKanbanDTO(kanban));
  }

  async getSingleKanban(kanbanId: number, userId: number): Promise<ReturnKanbanDTO> {
    const userFound = await this.findOneOrFailUser(userId);
    const kanban = await this.kanbansRepository.findOne({
      where: {
        id: kanbanId,
        isDeleted: false,
      },
      relations: ['kanbanCreator', 'lanes', 'invitedPeople'],
    });
    
    const person = kanban.invitedPeople.find(el => el.invitationEmail === userFound.email)
    if (
      userFound.id !== kanban.kanbanCreator.id   
      && kanban.isPublic === false   
      && (person === undefined) 
      ) {
        
      throw new BadRequestException('You can not access another users private kanban.')
    }
    return this.transformer.toReturnKanbanDTO(kanban);
  }

  async create(
    kanban: CreateKanbanDTO,
    userId: number,
  ): Promise<ReturnKanbanDTO> {
    const userFound = await this.findOneOrFailUserWithoutKanbans(userId);

    if (
      kanban.name === null ||
      kanban.name === undefined ||
      kanban.name === ''
    ) {
      throw new BadRequestException("Name can't be an empty field");
    }
    const createKanban = await this.kanbansRepository.create(kanban);
    createKanban.kanbanCreator = userFound;

    await this.kanbansRepository.save(createKanban);

    return this.transformer.toReturnKanbanDTO(createKanban);
  }

  async updateKanbanDetails( kanban: Partial<UpdateKanbanDTO>, kanbanId: number ): Promise<Partial<UpdateKanbanDTO>> {
    const kanbanToUpdate = await this.findOneOrFailKanban(kanbanId);
    if (kanban.name === kanbanToUpdate.name) {
      throw new BadRequestException(
        'There is already a kanban with this name. Names must be unique!',
      );
    }
    if (kanban.name === '') {
      throw new BadRequestException(
        'Kanban name can not be empty',
      );
    }
    await this.kanbansRepository.update(kanbanId, kanban);
    
    return kanban;
  }

  async delete(kanbanId: number, userId: number): Promise<ReturnMessageDTO> {
    const foundUser = await this.findOneOrFailUser(userId);
    const foundKanban = await this.kanbansRepository.findOne(kanbanId);

    if (!foundKanban) {
      throw new NotFoundException(`There is no kanban with id ${kanbanId}!`);
    }

    foundKanban.isDeleted = true;
    await this.kanbansRepository.save(foundKanban);

    return { message: `Kanban with id ${foundKanban.id} is deleted!` };
  }



  private async findOneOrFailUser(userId: number): Promise<User> {
    const user = await this.usersRepository.findOne(userId, {
      relations: ['kanbans'],
    });

    if (!user) {
      throw new Error('No user!');
    }

    return user;
  }

  private async findOneOrFailUserWithoutKanbans(userId: number): Promise<User> {
    const user = await this.usersRepository.findOne(userId);

    if (!user) {
      throw new Error('No user!');
    }

    return user;
  }

  private async findOneOrFailKanban(kanbanId: number): Promise<Kanban> {
    const kanban = await this.kanbansRepository.findOne(kanbanId);
    if (!kanban) {
      throw new NotFoundException('There is no such kanban!');
    }

    return kanban;
  }
}
