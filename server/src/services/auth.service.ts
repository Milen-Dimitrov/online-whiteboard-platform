import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { JWTPayload } from 'src/common/jwt-payload';
import * as bcrypt from 'bcrypt';
import { Token } from 'src/models/token.entity';
import { RecoverPassDTO } from 'src/dtos/users/recover-pass.dto';
import { NotFoundException } from '@nestjs/common/exceptions/not-found.exception';

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Token) private readonly tokenRepository: Repository<Token>,

        private readonly jwtService: JwtService,
    ) { }

    async findUserByName(email: string) {

        return await this.userRepository.findOne({
            where: {
                email,
            }
        });
    }

    async blacklist(token: string) {
        const tokenEntity = this.tokenRepository.create();
        tokenEntity.token = token;

        await this.tokenRepository.save(tokenEntity)
    }

    async isBlacklisted(token: string): Promise<boolean> {
        return Boolean(await this.tokenRepository.findOne({
            where: {
                token,
            }
        }));
    }

    async validateUser(email: string, password: string) {
        const user = await this.findUserByName(email);
        if (!user) {

            return null;
        }
        const isUserValidated = await bcrypt.compare(password, user.password);

        return isUserValidated
            ? user
            : null;
    }

    async login(email: string, password: string): Promise<{ token: string }> {
        const user = await this.validateUser(email, password);

        if (!user) {
            throw new UnauthorizedException('Wrong credentials!');
        }

        const payload: JWTPayload = {
            id: user.id,
            email: user.email,
            displayName: user.displayName,
        }

        const token = await this.jwtService.signAsync(payload);

        return {
            token,
        };
    }

    async forgotPassword(data: RecoverPassDTO): Promise<{ token: string }> {
        const user = await this.userRepository.findOne({
            where: {
                email: data.email
            }
        });
        if (!user) {
            throw new NotFoundException('There is no user with suck email')
        }
        if (user.secretAnswer !== data.secretAnswer) {
            throw new BadRequestException('Wrong answer')
        }
        
        const payload: JWTPayload = {
            id: user.id,
            email: user.email,
            displayName: user.displayName,
        }

        const token = await this.jwtService.signAsync(payload);

        return {
            token,
        };
    }
}
