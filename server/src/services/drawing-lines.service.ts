import { DrawingText } from './../models/text.entity';
import { ReturnTextDTO } from './../dtos/drawing-lines/return-text.dto';
import { TextDTO } from '../dtos/drawing-lines/text.dto';
import { User } from 'src/models/user.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { TransformService } from './transformer.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { DrawingLine } from 'src/models/drawing-line.entity';
import { Draw } from 'src/models/draw.entity';
import { CreateDrawingLineDTO } from 'src/dtos/drawing-lines/create-drawing-lines.dto';
import { ReturnDrawingLineDTO } from 'src/dtos/drawing-lines/retunr-drawing-lines.dto';

@Injectable()
export class DrawingLinesService {
  constructor(
    private readonly transformer: TransformService,
    @InjectRepository(DrawingLine)
    private readonly linesRepository: Repository<DrawingLine>,
    @InjectRepository(Draw)
    private readonly drawsRepository: Repository<Draw>,
    @InjectRepository(DrawingText)
    private readonly textRepository: Repository<DrawingText>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {}


  async create(line: CreateDrawingLineDTO, drawId: number): Promise<ReturnDrawingLineDTO> {
    const drawFound = await this.findOneOrFailDraw(drawId);

    const newLine = {
      tool: line.tool,
      color: line.color,
      points: line.points.join(',')
    }
    const createLine = this.linesRepository.create(newLine);

    createLine.drawParent = drawFound;

    const createdLine = await this.linesRepository.save(createLine);

    return this.transformer.toReturnDrawingLineDTO(createdLine);
  }

  async textCreate(text: TextDTO, drawId: number): Promise<ReturnTextDTO> {
    const drawFound = await this.findOneOrFailDraw(drawId);

    const createText = await this.textRepository.create(text);

    createText.textParent = drawFound;

    const createdText = await this.textRepository.save(createText);

    return this.transformer.toReturnTextDTO(createdText);
  }

  private async findOneOrFailDraw(drawId: number): Promise<Draw> {
    const draw = await this.drawsRepository.findOne(drawId, {
      relations: ['lines'],
    });

    if (!draw) {
      throw new NotFoundException('No Draw!');
    }

    return draw;
  }
}
