import { Lanes } from './../models/lanes.entity';
import { ReturnLaneDTO } from 'src/dtos/lanes/return-lane.dto';
import { User } from 'src/models/user.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { TransformService } from './transformer.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ReturnMessageDTO } from 'src/dtos/return-message/return-message.dto';
import { Kanban } from 'src/models/kanban.entity';
import { CreateLanesDTO } from 'src/dtos/lanes/create-lanes.dto';
import { UpdateLaneDTO } from 'src/dtos/lanes/update-lane.dto';

@Injectable()
export class LanesService {
  constructor(
    private readonly transformer: TransformService,
    @InjectRepository(Lanes)
    private readonly lanesRepository: Repository<Lanes>,
    @InjectRepository(Kanban)
    private readonly kanbansRepository: Repository<Kanban>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {}

  async getSingleLane(laneId: number): Promise<ReturnLaneDTO> {
    const lane = await this.lanesRepository.findOne({
      where: {
        id: laneId,
        isDeleted: false,
      },
      relations: ['parentKanban', 'cards'],
    });
    return this.transformer.toReturnLaneDTO(lane);
  }

  async create({ kanbanId, ...lane }: CreateLanesDTO): Promise<ReturnLaneDTO> {
    const kanbanFound = await this.findOneOrFailKanban(kanbanId);
    const createLane = this.lanesRepository.create(lane);

    createLane.parentKanban = kanbanFound;

    const createdLane = await this.lanesRepository.save(createLane);

    return this.transformer.toReturnLaneDTO(createdLane);
  }

  async update(lane: UpdateLaneDTO, laneId: number): Promise<ReturnLaneDTO> {
    const laneFound = await this.lanesRepository.findOne(laneId);

    if (!laneFound) {
      throw new NotFoundException('Lane with this ID does not exist');
    }

    await this.lanesRepository.update(laneId, lane);

    return this.transformer.toReturnLaneDTO(laneFound);
  }

  async delete(laneId: number): Promise<ReturnMessageDTO> {
    const laneFound = await this.lanesRepository.findOne(laneId);

    if (!laneFound) {
      throw new NotFoundException('Lane with this ID does not exist');
    }

    laneFound.isDeleted = true;
    await this.lanesRepository.save(laneFound);

    return { message: `Lane with id ${laneFound.id} is deleted!` };
  }

  private async findOneOrFailKanban(kanbanId: number): Promise<Kanban> {
    const kanban = await this.kanbansRepository.findOne(kanbanId, {
      relations: ['lanes'],
    });

    if (!kanban) {
      throw new NotFoundException('No Kanban!');
    }

    return kanban;
  }
}
