import createRequest, { GET, PUT, POST, DELETE } from './utils';

export const BASE_URL = 'http://localhost:3000';

const login = (data) => {
  return createRequest(`${BASE_URL}/session`, POST, data);
};

const register = (data) => {
  return createRequest(`${BASE_URL}/users`, POST, data);
};

const recover = (data) => {
  return createRequest(`${BASE_URL}/session`, PUT, data);
};

const getUserById = (id) => {
  return createRequest(`${BASE_URL}/users/${id}`, GET);
};

const updateUserDisplayName = (id, data) => {
  return createRequest(`${BASE_URL}/users/${id}`, PUT, data);
};
const updateUserPassword = (id, data) => {
  return createRequest(`${BASE_URL}/users/${id}/pass`, PUT, data);
};

const createKanban = (data) => {
  return createRequest(`${BASE_URL}/kanbans`, POST, data);
};

const createDraw = (data) => {
  return createRequest(`${BASE_URL}/draws`, POST, data);
};

const getKanbanById = (kanbanId) => {
  return createRequest(`${BASE_URL}/kanbans/${kanbanId}`, GET);
};

const getPrivateKanbans = () => {
  return createRequest(`${BASE_URL}/kanbans/private`, GET);
};
const getPublicKanbans = () => {
  return createRequest(`${BASE_URL}/kanbans/public`, GET);
};
const getPrivateDraws = () => {
  return createRequest(`${BASE_URL}/draws/private`, GET);
};

const getPublicDraws = () => {
  return createRequest(`${BASE_URL}/draws/public`, GET);
};

const getAllPublicKanbans = () => {
  return createRequest(`${BASE_URL}/kanbans`, GET)
};

const getAllPublicDraws = () => {
  return createRequest(`${BASE_URL}/draws`, GET)
};

const drawUpdate = (id, data) => {
  return createRequest(`${BASE_URL}/draws/${id}`, PUT, data);
};

const kanbanUpdate = (id, data) => {
  return createRequest(`${BASE_URL}/kanbans/${id}`, PUT, data);
};  

const invite = (data, kanbanId) => {
  return createRequest(`${BASE_URL}/invite/kanbans/${kanbanId}`, POST, data);
};
const inviteToDraw = (data, drawId) => {
  return createRequest(`${BASE_URL}/invite/draws/${drawId}`, POST, data);
};

const kanbanDelete = (id) => {
  return createRequest(`${BASE_URL}/kanbans/${id}`, DELETE);
};

const drawDelete = (id) => {
  return createRequest(`${BASE_URL}/draws/${id}`, DELETE);
};

const createLane = (data) => {
  return createRequest(`${BASE_URL}/lanes`, POST, data);
};

const updateLane = (lanesId, data) => {
  return createRequest(`${BASE_URL}/lanes/${lanesId}`, PUT, data)
};

const deleteLane = (laneId) => {
  return createRequest(`${BASE_URL}/lanes/${laneId}`, DELETE);
};

const createCard = (laneId, card) => {
  return createRequest(`${BASE_URL}/lanes/${laneId}/cards`, POST, card);
};

const updateCardPosition = (card, cardId) => {
  return createRequest(`${BASE_URL}/lanes/cards/${cardId}/`, PUT, card);
};

const deleteCard = (cardId) => {
  return createRequest(`${BASE_URL}/lanes/cards/${cardId}`, DELETE)
};

const getDrawById = (id) => {
  return createRequest(`${BASE_URL}/draws/${id}`, GET);
};

const createDrawingLine = (id, data) => {
  return createRequest(`${BASE_URL}/draws/${id}/lines`, POST, data);
};

const createDrawingText = (id, data) => {
  return createRequest(`${BASE_URL}/draws/${id}/text`, POST, data);
};

export default {
  login,
  register,
  recover,
  getUserById,
  updateUserDisplayName,
  updateUserPassword,
  createKanban,
  createDraw,
  getKanbanById,
  getAllPublicKanbans,
  getPrivateKanbans,
  getPublicKanbans,
  getPublicDraws,
  getPrivateDraws,
  getAllPublicDraws,
  kanbanUpdate,
  drawUpdate,
  drawDelete,
  kanbanDelete,
  invite,
  inviteToDraw,
  createLane,
  updateLane,
  deleteLane,
  createCard,
  updateCardPosition,
  deleteCard,
  getDrawById,
  createDrawingLine,
  createDrawingText
};
