export const GET = 'GET'
export const PUT = 'PUT'
export const POST = 'POST'
export const DELETE = 'DELETE'

const createRequest = (url, method, data) => {
  return fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.error) {
        throw new Error('Ooops something went wrong :(')
      }

      return result
    })
    .catch((error) => {
      console.error(error.message)
    })
}

export default createRequest;
