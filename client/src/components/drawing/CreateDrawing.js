import React, { useState } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import api from '../../api';
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBInput, MDBBtn } from 'mdbreact';

const CreateDrawing = () => {
  const history = useHistory();
  const [draw, setDraw] = useState({ name: '', isPublic: true });

  const onDrawChange = (key) => ({ target: {value} }) => {
    setDraw({ ...draw, [key]: value });
  };

  const addDraw = () => {
    if(draw.isPublic === 'false'){
      draw.isPublic = false
    }
    api.createDraw(draw).then(
      history.push(`/drawing/my`)
   );
  };

  return (
    <MDBContainer>
      <MDBRow>
        <MDBCol>
          <MDBCard style={{ margin: '5rem auto', width: '520px' }}>
            <MDBCardBody className='mx-4'>
              <div className='text-center'>
                <h3 className='dark-grey-text mb-5'>
                  <strong>Create New Draw</strong>
                </h3>
              </div>

              <MDBContainer className='mt-5 p-0'>
                <select className='browser-default custom-select' onChange={onDrawChange('isPublic')}>
                  <option value={true}>Public</option>
                  <option value={false}>Private</option>
                </select>
                <MDBInput
                  labelClass='mx-2'
                  label='Name'
                  group
                  type='text'
                  validate
                  containerClass='mb-0'
                  value={draw.name}
                  onChange={onDrawChange('name')}
                />

                <MDBBtn color='primary' rounded className='btn-primary btn-block z-depth-1a' onClick={addDraw}>
                  Create
                </MDBBtn>
              </MDBContainer>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default withRouter(CreateDrawing);
