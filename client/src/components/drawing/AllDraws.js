import React, { useState, useEffect, useContext } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import api from '../../api';
import DrawItem from './DrawItem';
import SingleDraw from './SingleDraw';
import { notify } from '../common/Notification';

const AllDraws = () => {
  const [draws, setDraws] = useState([]);
  const [, setError] = useState(null);
  const [currentDraw, setCurrentDraw] = useState({});


  const { loggedUser } = useContext(AuthContext);
  const [privacyState, setPrivacyState] = useState(true);
  const changePrivacy = () => setPrivacyState(!privacyState);

  useEffect(() => {
    api
      .getAllPublicDraws()
      .then((draws) => {
        setDraws(draws);
      })
      .catch((error) => setError(error.message));
    return window.removeEventListener('', api.getAllPublicDraws);
  }, []);

  const deleteDraw = (id) => {
    const foundDraw = draws.find((d) => d.id === id);
    if (foundDraw.drawCreator.id !== loggedUser.id) {
      return notify(`You don't have the rights to delete the draw!`);
    }

    api
      .drawDelete(id)
      .then((status) => {
        alert('You have just deleted this draw!');
      })
      .catch((error) => setError(error.message));

    const newDraws = draws.filter((d) => d.id !== id);
    setDraws(newDraws);
  };

  const updateDraw = (id, data) => {
    api.drawUpdate(id, data)
      .then((result) => {

        if (result.error) {
          notify(result.message)
        }

        console.log(result);
        setCurrentDraw({...result})
        setDraws(draws.map(draw => draw.id === id ? {...draw, ...result}: draw))

        notify(`Name changed`)

      })
      .catch((error) => setError(error.message));
  };

  const updateAccess = (id, data) => {
    api.drawUpdate(id, data)
      .then((result) => {
        if (result.error) {
          notify(result.message)
        }
        setDraws(draws.map(draw => draw.id === id ? { ...draw, ...result } : draw))
        changePrivacy()
        notify(`Status changed`)
      })
      .catch((error) => setError(error.message));
  };

  return (
    <div className='d-flex' style={{ padding: '0.5rem' }}>
      <Switch>
        <Route path='/drawing/all/:id' component={SingleDraw} />

        <Route
          path='/drawing/all'
          render={() =>
            draws?.map((d) => (
              <DrawItem
                key={d.id}
                drawKey={d.id}
                name={d.name}
                drawCreator={d.drawCreator}
                deleteDraw={deleteDraw}
                loggedUser={loggedUser}
                updateDraw={updateDraw}
                currentDraw={currentDraw}
                setCurrentDraw={setCurrentDraw}
                updateAccess={updateAccess}
                draw={d}
              />
            ))
          }
        />
      </Switch>
    </div>
  );
};

export default withRouter(AllDraws);
