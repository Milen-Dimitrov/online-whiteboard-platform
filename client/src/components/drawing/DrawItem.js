import React, {
  useContext,
  useState
} from 'react';
import propTypes from 'prop-types';
import AuthContext from '../../providers/AuthContext';
import ThemeContext from '../../providers/ThemeContext';
import { withRouter, useLocation } from 'react-router-dom';
import { MDBCard, MDBCardBody, MDBCardImage, MDBCardHeader, MDBCardText, MDBLink, MDBIcon, MDBInput, MDBBtn, MDBCardFooter, MDBTooltip } from 'mdbreact';
import api from '../../api';
import { notify } from '../common/Notification'


const DrawItem = ({ draw, currentDraw, setCurrentDraw, deleteDraw, updateDraw, updateAccess }) => {

  const location = useLocation();
  const isAllDrawing = location.pathname.includes('all');
  const { loggedUser } = useContext(AuthContext);
  const { themeProps } = useContext(ThemeContext);

  const [updatedName, setUpdatedName] = useState({
    name: ''
  });
  const [inviteState, setInviteState] = useState(true);
  const [updatedInvite, setUpdatedInvite] = useState({
    invitationEmail: ''
  });
  const id = draw.id;

  const [privacyState, setPrivacyState] = useState(true);
  const [updatedPrivacy, setUpdatedPrivacy] = useState({
    isPublic: true
  });

  const [, setError] = useState(null);

  const changePrivacy = () => setPrivacyState(!privacyState);

  const updateDrawPrivacy = (prop, value) => {
    setUpdatedPrivacy({
      [prop]: value
    })
  };

  const updateDrawName = (prop, value) =>
    setUpdatedName({
      ...updatedName,
      [prop]: value
    });

  const inviteSomeone = () => api.inviteToDraw(updatedInvite, id)
    .then((result) => {
      notify(`${result.message}`);
      if (result.error) {
        notify(result.message)
      }
    })
    .catch((error) => setError(error.message))
    .then(setInviteState(!inviteState))

  const changeInviteState = () => {
    setInviteState(!inviteState);
  };
  const updateInviteName = (prop, value) =>
    setUpdatedInvite({
      ...updatedInvite,
      [prop]: value
    });

  return (
    <MDBCard style={{ width: '16rem', margin: '0.5rem' }}>
      <MDBCardHeader>
        {loggedUser.id === draw.drawCreator.id && !isAllDrawing ? (
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <MDBTooltip domElement tag='span' placement='top' >
              <span onClick={() => changeInviteState()}>
                <MDBIcon icon='user-plus' onClick={() => changeInviteState()} />
              </span>
              <div>Invite</div>
            </MDBTooltip>
            <div style={{ position: 'relative' }}>
              <MDBTooltip domElement tag='span' placement='top'>
                <span onClick={() => deleteDraw(id)}>
                  <MDBIcon far icon='trash-alt' />
                </span>
                <div>Delete</div>
              </MDBTooltip>
            </div>
          </div>
        )
          : null
        }

        {/* //</MDBCardHeader> <MDBCard style={{ width: '16rem', margin: '0.5rem', position: "relative", display: "flex", justifyContent: 'space-between', flexDirection: 'column' }}>
      //   <MDBCardHeader >
      //     {loggedUser.id === draw.drawCreator.id ?
      //       <MDBIcon far icon="trash-alt" style={{ position: "absolute", top: '8px', right: "3px" }} onClick={() => deleteDraw(id)} />
      //       : null
      //     } */}
        <MDBCardImage className='img-fluid p-3' src={draw.drawCreator.picture} style={{ borderRadius: '50%' }} waves />

        {inviteState ? (
          <div style={{ position: 'relative' }}>
            <h6 className='font-weight-bold my-2 text-center'>
              {draw.name}
              <MDBTooltip domElement tag='span' placement='right'>
                <span onClick={() => setCurrentDraw(draw.id !== currentDraw?.id ? draw : {})}>
                  <MDBIcon far icon='edit' className='ml-1' />
                </span>
                <div>Edit name</div>
              </MDBTooltip>
            </h6>
          </div>
        ) : (
            <>
              <MDBInput hint="Your e-mail" type="email"
                onChange={(e) => updateInviteName('invitationEmail', e.target.value)} >
              </MDBInput>
              <MDBBtn  {...themeProps} onClick={inviteSomeone}>Invite</MDBBtn>
            </>
          )}
      </MDBCardHeader>
      <MDBCardBody className='p-3'>

        {/* //     <h6 className='font-weight-bold'>
        //       {draw.name} 
        //     <MDBIcon far icon="edit" className='ml-1' 
        //     onClick={() => setCurrentDraw(draw.id !== currentDraw?.id ? draw: {})} /> 
        //     </h6>
        //     : <>
        //       <MDBInput hint="Your e-mail" type="email"
        //         onChange={(e) => updateInviteName('invitationEmail', e.target.value)} >
        //       </MDBInput>
        //       <MDBBtn  {...themeProps} onClick={inviteSomeone}>Invite</MDBBtn>
        //     </>}
        //     </MDBCardHeader>
        // <MDBCardBody className='p-3'> */}

        {draw.id !== currentDraw?.id
          ? null
          : (
            <>
              <input type='text'
                id='edit-draw-name'
                placeholder={`Type new name`}
                onChange={(e) => updateDrawName('name', e.target.value)}
              />
              <MDBBtn {...themeProps} onClick={() => updateDraw(id, updatedName)}>Submit</MDBBtn>
            </>
          )}
        <MDBCardText>Draw creator: {draw.drawCreator.displayName}</MDBCardText>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <MDBCardText>Draw status: {draw.isPublic ? `Public` : `Private`}
          <MDBTooltip domElement tag='span' placement='top' >
              <span onClick={() => changePrivacy()}>
                <MDBIcon icon='sync' hover={`switch status`} className='ml-2' />
              </span>
              <div>Change status</div>
            </MDBTooltip>
          </MDBCardText>
        </div>
          {privacyState
            ? null
            :
            <div className='custom-control custom-switch'>
              <select className='browser-default custom-select' onChange={(e) => updateDrawPrivacy('isPublic', e.target.value)}>
                <option value={true}>Public</option>
                <option value={false}>Private</option>
              </select>
              <MDBBtn {...themeProps} onClick={() => updateAccess(id, updatedPrivacy)} >Update</MDBBtn>
            </div>
          }

          {/* {loggedUser.id === draw.drawCreator.id
            ? <MDBIcon icon="user-plus" style={{ position: "absolute", left: "8px", top: "8px" }} onClick={() => changeInviteState()} />
            : null
          </div>
        } */}
        </MDBCardBody>
      <MDBCardFooter>
        <MDBLink {...themeProps} to={`all/${id}`} className='btn rounded w-100 p-2 mx-0'>
          Show
          </MDBLink>
      </MDBCardFooter>
    </MDBCard>
  );
};

DrawItem.propTypes = {
  draw: propTypes.object.isRequired
};

export default withRouter(DrawItem);
