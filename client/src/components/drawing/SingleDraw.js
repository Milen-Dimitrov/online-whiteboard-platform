import React, { useState, useRef, useEffect } from 'react';
import { Stage, Layer, Text, Line } from 'react-konva';
import { withRouter, useParams, useHistory } from 'react-router-dom';
import api from '../../api';
import { CompactPicker } from 'react-color';
import { MDBIcon } from 'mdbreact';
import { notify } from '../common/Notification';


const nextId = ((id = 1) => () => id++)();
const defaultText = {
  textEditVisible: true,
  textX: 400,
  fill: 'black',
  textY: 300,
  textValue: '',
  fontSize: 8,
  width: 400,
  fontStyle: 'normal',
  align: 'left',
  id: null
};

const SingleDraw = () => {
  const { id } = useParams();
  const history = useHistory();
  const [tool, setTool] = useState('pen');
  const [isDrawing, toggleDrawing] = useState(false);
  const [lines, setLines] = useState([]);
  const inputRef = useRef();
  const [currentText, setCurrentText] = useState({});
  const [textList, setTextList] = useState([]);
  const [colorPicker, setColoricker] = useState(false)
  const [backgroundColor, setBackgroundColor] = useState({
    background: '#RRGGBB'
  });
  

  const drawingLineAdd = (data) => api.createDrawingLine(id, data).then(console.log);

  useEffect(() => inputRef.current.focus());

  useEffect(() => {
    api.getDrawById(id)
      .then((result) => {
        if (result.error) {
          notify(`${result.message}`)
          history.push(`/draw/all`)
        } else {
          setLines(result.lines)
          setTextList(result.text)
        }

      })
      .catch((error) => console.error(error.message))
    return window.removeEventListener('', api.getDrawById);
  }, []);

  const onCreateText = () => setCurrentText(defaultText);
  const onChangeText = (e) => setCurrentText({ ...currentText, textValue: e.target.value });
  const onDblClickText = (id) => (e) => setCurrentText({ ...textList.find((t) => t.id === id), textEditVisible: true });
  const onSaveText = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();

      const newText = { ...currentText, textEditVisible: false, id: currentText.id || nextId() };
      setTextList(textList.filter((t) => t.textValue && t.id !== newText.id).concat(newText.textValue.length ? newText : []));
      setCurrentText({ ...defaultText, textEditVisible: false });
      api.createDrawingText(id, currentText).then(console.log)
    }
  };

  const showColorPicker = () => {
    setColoricker(!colorPicker)
  }

  const handleChangeComplete = (color) => {
    setBackgroundColor({ background: color.hex });
    showColorPicker()
  };

  return (
    <div>

      <MDBIcon icon="spell-check" size="2x" className="dark-grey-text pr-3" onClick={onCreateText} />

      <MDBIcon icon="pen" size="2x" className="dark-grey-text pr-3" value={tool}
        onClick={() => {
          setTool('pen');
        }} />

      <MDBIcon icon="paint-brush" size="2x" className="dark-grey-text pr-3" value={tool}
        onClick={() => {
          setTool('brush');
        }} />

      <MDBIcon icon="eraser" size="2x" className="dark-grey-text pr-3" value={tool}
        onClick={() => {
          setTool('eraser');
        }} />

      <MDBIcon icon="palette" size="2x" className="dark-grey-text pr-3" onClick={showColorPicker} />
      { colorPicker && <div><CompactPicker color={backgroundColor} onChangeComplete={handleChangeComplete} /></div>}

      <Stage
        width={window.innerWidth}
        height={window.innerHeight}
        onMouseDown={(e) => {
          toggleDrawing(true);
          const pointer = e.target.getStage().getPointerPosition();
          const newLines = lines.concat({
            id: Date.now(),
            tool: tool,
            color: backgroundColor.background,
            points: [pointer.x, pointer.y]
          });
          setLines(newLines);
        }}
        onMouseMove={(e) => {
          if (!isDrawing) {
            return;
          }
          const pointer = e.target.getStage().getPointerPosition();
          const newLines = [...lines];
          const lastLine = {
            ...newLines[newLines.length - 1]
          };
          lastLine.points = lastLine.points.concat([pointer.x, pointer.y]);
          newLines[newLines.length - 1] = lastLine;
          setLines(newLines);
        }}
        onMouseUp={(e) => {
          toggleDrawing(false);
          const last = lines[lines.length - 1];
          drawingLineAdd({
            tool: last.tool,
            color: last.color,
            points: last.points
          })
        }}>
        <Layer>
          {lines.map((line) => (
            <Line
              key={line.id}
              strokeWidth={line.tool === 'pen' ? 1 : line.tool === 'brush' ? 7 : line.tool === 'eraser' ? 10 : 1 }
              stroke={line.color}
              points={line.points}
              globalCompositeOperation={line.tool === 'eraser' ? 'destination-out' : 'source-over'}
            />
          ))}
          {textList.map((t) => (
            <Text
              key={t.id}
              fontSize={24}
              align={'left'}
              fontStyle={20}
              draggable
              text={t.textValue}
              x={100}
              y={100}
              wrap='word'
              width={t.width}
              onDblClick={onDblClickText(t.id)}
            />
          ))}
        </Layer>
      </Stage>
      <textarea
        ref={inputRef}
        value={currentText.textValue}
        style={{
          display: currentText.textEditVisible ? 'block' : 'none',
          position: 'absolute',
          top: '50%',
          left: '50%',
          width: '30%',
          height: '20%',
          transform: 'translate(-50%,-50%)',
          fontSize: '2em',
          padding: '1rem'
        }}
        onChange={onChangeText}
        onKeyDown={onSaveText}
      />
    </div>
  );
};

export default withRouter(SingleDraw);
