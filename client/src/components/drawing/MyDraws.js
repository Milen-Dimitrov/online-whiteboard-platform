import React, { useState, useEffect, useContext } from 'react';
import { MDBNav, MDBNavItem, MDBBtn } from 'mdbreact';
import AuthContext from '../../providers/AuthContext';
import api from '../../api';
import { notify } from '../common/Notification';
import SingleDraw from './SingleDraw';
import DrawItem from './DrawItem';

const MyDraws = () => {
  const [privateDraws, setPrivateDraws] = useState([]);
  const [publicDraws, setPublicDraws] = useState([]);
  const [currentDraw, setCurrentDraw] = useState({});

  const { loggedUser } = useContext(AuthContext);

  const [showPrivate, setShowPrivate] = useState(false);
  const [, setError] = useState(null);
  const [privacyState, setPrivacyState] = useState(true);

  const changePrivacy = () => setPrivacyState(!privacyState);


  useEffect(() => {
    api.getPrivateDraws().then(setPrivateDraws);

    return window.removeEventListener('', api.getPrivateDraws);
  }, [privacyState]);

  useEffect(() => {
    api.getPublicDraws().then(setPublicDraws);
    return window.removeEventListener('', api.getPublicDraws);
  }, [privacyState]);

  const drawToDelete = (id) => {
    if (showPrivate) {
      const foundPrivate = privateDraws.find((d) => d.id === id);

      if (foundPrivate.drawCreator.id !== loggedUser.id) {
        return notify(`You don't have the rights to delete the draw!`);
      }
      api.drawDelete(id).then(notify(`You have just deleted this draw!`));
      const newDraws = privateDraws.filter((d) => d.id !== id);
      setPrivateDraws(newDraws);
    } else {
      const foundPublic = publicDraws.find((d) => d.id === id);
      if (!foundPublic) {
        return notify(`No draw with id ${id} found!`);
      }

      if (foundPublic.drawCreator.id !== loggedUser.id) {
        return notify(`You don't have the rights to delete the draw!`);
      }
      api.drawDelete(id).then(notify(`You have just deleted this draw!`));
      const newDraws = publicDraws.filter((d) => d.id !== id);
      setPublicDraws(newDraws);
    }
  };

  const updateDraw = (id, data) => {
    api.drawUpdate(id, data)
      .then((result) => {

        if (result.error) {
          notify(result.message)
        }

        console.log(result);
        setCurrentDraw({...result})
        setPrivateDraws(privateDraws.map(draw => draw.id === id ? {...draw, ...result}: draw))
        setPublicDraws(publicDraws.map(draw => draw.id === id ? {...draw, ...result}: draw))

        notify(`Name changed`)

      })
      .catch((error) => setError(error.message));
  };

  const updateAccess = (id, data) => {
    api.drawUpdate(id, data)
      .then((result) => {
        if (result.error) {
          notify(result.message)
        }
        setPrivateDraws(privateDraws.map(draw => draw.id === id ? {...draw, ...result}: draw))
        setPublicDraws(publicDraws.map(draw => draw.id === id ? {...draw, ...result}: draw))
        changePrivacy()
        notify(`Status changed`)
      })
      .catch((error) => setError(error.message));
  };

  return (
    <div>
      <MDBNav className='mb-2' tabs>
        <MDBNavItem>
          <MDBBtn size='sm' color='primary' active={!showPrivate} onClick={() => setShowPrivate(false)} role='tab'>
            Public
          </MDBBtn>
        </MDBNavItem>
        <MDBNavItem>
          <MDBBtn size='sm' color='primary' active={showPrivate} onClick={() => setShowPrivate(true)} role='tab'>
            Private
          </MDBBtn>
        </MDBNavItem>
      </MDBNav>
      <div style={{ display: "flex", flexFlow: "row wrap" }}>
      {showPrivate
        ? privateDraws?.map((d) => (
          <DrawItem
            key={d.id}
            drawKey={d.id}
            name={d.name}
            drawCreator={d.drawCreator}
            deleteDraw={drawToDelete}
            loggedUser={loggedUser}
            updateDraw={updateDraw}
            currentDraw={currentDraw}
            setCurrentDraw={setCurrentDraw}
            updateAccess={updateAccess}
            draw={d}
          />
        ))
        : publicDraws?.map((d) => (
          <DrawItem
            key={d.id}
            drawKey={d.id}
            name={d.name}
            kanbanCreator={d.kanbanCreator}
            deleteDraw={drawToDelete}
            loggedUser={loggedUser}
            updateDraw={updateDraw}
            currentDraw={currentDraw}
            setCurrentDraw={setCurrentDraw}
            updateAccess={updateAccess}
            draw={d}
          />
        ))}
        </div>
    </div>
  );
};

export default MyDraws;
