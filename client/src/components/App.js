import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AuthContext, { extractUser, getToken } from '../providers/AuthContext';
import ThemeContext, { getThemeProps } from '../providers/ThemeContext';
import GuardedRoute from '../providers/GuardedRoute';
import WhiteboardPage from './page/WhiteboardPage';
import SignInPage from './page/SignInPage';
import ProfilePage from './page/ProfilePage';
import DrawingPage from './page/DrawingPage';
import KanbanPage from './page/KanbanPage';
import { MDBFooter, MDBLink, MDBNavItem } from 'mdbreact';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBIcon } from 'mdbreact';

const App = () => {
  const [authValue, setAuthValue] = useState({ isLoggedIn: !!extractUser(getToken()), loggedUser: extractUser(getToken()) });
  const [isOpen, setIsOpen] = useState(false);
  const [isDark, setIsDark] = useState(false);

  const logout = (e) => {
    e.preventDefault();
    localStorage.removeItem('token');

    setAuthValue({ isLoggedIn: false, loggedUser: null });
  };

  const toggleCollapse = () => {
    setIsOpen(!isOpen);
  };

  const toggleTheme = () => {
    setIsDark(!isDark);
  };

  const themeProps = getThemeProps(isDark);

  return (
    <BrowserRouter>
      <ThemeContext.Provider value={{ themeProps }}>
        <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
          <div
            role='navigation'
            style={{ minHeight: 'calc(100vh - 29px)', width: '100%', backgroundColor: isDark ? '#3E4551' : '#FEFEFE' }}>
            <MDBNavbar {...themeProps} className='px-3' expand='md'>
              <MDBNavbarBrand>
                <MDBNavLink className='btn-outline text-white' to='/whiteboard'>
                  Whiteboard
                </MDBNavLink>
              </MDBNavbarBrand>
              <MDBNavbarToggler onClick={toggleCollapse} />
              <MDBCollapse id='navbarCollapse3' isOpen={isOpen} navbar>
                {authValue.loggedUser && (
                  <MDBNavbarNav left>
                    <MDBNavLink className='btn-outline text-white' to='/drawing/all'>
                      Drawing
                    </MDBNavLink>
                    <MDBNavLink className='btn-outline text-white' to='/kanban/all'>
                      Kanban
                    </MDBNavLink>
                  </MDBNavbarNav>
                )}
                <MDBNavbarNav right>
                  <MDBNavItem className='m-auto'>
                    <div className='text-white custom-control custom-switch mr-3'>
                      <div className='custom-control custom-switch'>
                        <input
                          type='checkbox'
                          className='custom-control-input'
                          id='customSwitches'
                          checked={isDark}
                          onChange={toggleTheme}
                          readOnly
                        />
                        <label className='custom-control-label hoverable' htmlFor='customSwitches' style={{ userSelect: 'none' }}>
                          <MDBIcon icon='adjust' size='1x' />
                        </label>
                      </div>
                    </div>
                  </MDBNavItem>
                  {!authValue.loggedUser ? (
                    <MDBNavLink className='btn-outline text-white' to='/signin'>
                      Sign In
                    </MDBNavLink>
                  ) : (
                    <>
                      <MDBLink className='btn-outline text-white font-weight-bold' to='/profile'>
                        {authValue.loggedUser.displayName}
                      </MDBLink>
                      <MDBLink className='btn-outline text-white' onClick={logout} to='/whiteboard'>
                        logout
                      </MDBLink>
                    </>
                  )}
                </MDBNavbarNav>
              </MDBCollapse>
            </MDBNavbar>
            <Switch>
              <Route path='/whiteboard' exact component={WhiteboardPage} />
              <Route path='/signin' exact component={SignInPage} />
              <GuardedRoute path='/drawing' auth={authValue.isLoggedIn} component={DrawingPage} />
              <GuardedRoute path='/kanban' auth={authValue.isLoggedIn} component={KanbanPage} />
              <GuardedRoute path='/profile' exact auth={authValue.isLoggedIn} component={ProfilePage} />
            </Switch>
            <MDBFooter {...themeProps} style={{ position: 'fixed', width: '100%', textAlign: 'center', padding: '0.15rem', bottom: '0' }}>
              Whiteboard 2020
            </MDBFooter>
          </div>
        </AuthContext.Provider>
      </ThemeContext.Provider>
    </BrowserRouter>
  );
};

export default App;
