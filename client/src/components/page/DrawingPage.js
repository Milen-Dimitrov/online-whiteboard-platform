import React, { useContext } from 'react';
import { Switch, Route } from 'react-router-dom';
import { MDBRow, MDBCol, MDBCardImage, MDBCard, MDBCardBody, MDBNavLink } from 'mdbreact';
import CreateDrawing from '../drawing/CreateDrawing';
import AllDraws from '../drawing/AllDraws';
import ThemeContext from '../../providers/ThemeContext';
import Chat from '../common/Chat';
import MyDraws from '../drawing/MyDraws';

const DrawingPage = () => {
  const { themeProps } = useContext(ThemeContext);

  return (
    <MDBRow className='m-0 py-3'>
      <MDBCol style={{ minWidth: '320px', maxWidth: '320px' }}>
        <MDBCard style={{ height: 'calc(100vh - 136px)' }} className='m-0'>
          <MDBCardImage className='text-center rounded border-bottom pt-3 pb-2' tag='div'>
            <h4>Drawing Board</h4>
          </MDBCardImage>
          <MDBCardBody cascade className='text-center h-100 p-3'>
            <MDBNavLink {...themeProps} className='btn' to='/drawing/all'>
              ALL DRAWINGS
            </MDBNavLink>
            <MDBNavLink {...themeProps} className='btn' to='/drawing/my'>
              MY DRAWINGS
            </MDBNavLink>
            <MDBNavLink {...themeProps} className='btn' to='/drawing/create'>
              CREATE DRAWING
            </MDBNavLink>
          </MDBCardBody>
          <Chat room={'Draw'} />
        </MDBCard>
      </MDBCol>
      <MDBCol md='9'>
        <MDBCard style={{ height: 'calc(100vh - 136px)', overflowY: 'scroll', minWidth: '300px' }} className='m-0'>
          <Switch>
            <Route path='/drawing/all' component={AllDraws} />
            <Route path='/drawing/my' exact component={MyDraws} />
            <Route path='/drawing/create' exact component={CreateDrawing} />
          </Switch>
        </MDBCard>
      </MDBCol>
    </MDBRow>
  );
};

export default DrawingPage;
