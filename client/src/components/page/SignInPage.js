import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AuthContext, { extractUser } from '../../providers/AuthContext';
import ThemeContext from '../../providers/ThemeContext';
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBInput, MDBBtn, MDBModalFooter } from 'mdbreact';
import api from '../../api';
import { notify } from '../common/Notification';

const SignIn = () => {
  const [isMember, setIsMember] = useState(true);
  const history = useHistory();

  const { setLoginState } = useContext(AuthContext);
  const { themeProps } = useContext(ThemeContext);
  const [passRecover, setPassRecover] = useState(false)

  const changeRecoverState = () =>  setPassRecover(!passRecover)

  const [user, setUserObject] = useState({
    email: '',
    displayName: '',
    password: '',
    confirmPassword: '',
    secretAnswer: ''
  });

  const updateUser = (prop, value) => setUserObject({ ...user, [prop]: value });

  const login = () => {
    if (!user.email) {
      return notify('Invalid email!');
    }
    if (!user.password) {
      return notify('Invalid password!');
    }

    api
      .login(user)
      .then((result) => {
        try {
          setLoginState({ isLoggedIn: true, loggedUser: extractUser(result.token) });
        } catch (e) {
          return notify(`Invalid credentials!`);
        }

        localStorage.setItem('token', result.token);
        notify(`Welcome`)
        history.push('/profile');
        
      })
      .catch();
  };

  const register = () => {
    if (!user.email) {
      return notify('Invalid email!');
    }
    if (!user.password) {
      return notify('Invalid password!');
    }
    if (user.password !== user.confirmPassword) {
      return notify("Passwords doesn't match !");
    }

    api
      .register({ ...user, displayName: user.displayName })
      .then(() => setIsMember(true))
      .catch();
      notify(`Registration successful!`)
  };

  const logWithRecoveredPassword = () => {
    if (!user.email) {
      return notify('Invalid email!');
    }
    if (!user.secretAnswer) {
      return notify('Invalid secret answer!');
    }
    api
      .recover({email: user.email, secretAnswer: user.secretAnswer})
      .then((result) => {
        try {
          setLoginState({ isLoggedIn: true, loggedUser: extractUser(result.token) });
        } catch (e) {
          return notify(`Invalid credentials!`);
        }

        localStorage.setItem('token', result.token);
        history.push('/profile');
      })
      .catch();
  }

  return (
    <MDBContainer>
      <MDBRow>
        <MDBCol>
          <MDBCard style={{ margin: '1rem auto', width: '420px' }}>
            <MDBCardBody className='mx-4'>
              <div className='text-center'>
                <h3 className='dark-grey-text mb-4'>
                  <strong>{isMember ? 'Sign In' : 'Sign Up'}</strong>
                </h3>
              </div>

              {isMember ? (
                <div className='text-center mb-3'>
                  <MDBInput
                    label='Email'
                    group
                    type='email'
                    validate
                    error='wrong'
                    success='right'
                    value={user.email}
                    onChange={(e) => updateUser('email', e.target.value)}
                  />

                  {passRecover? <>
                  <MDBInput
                    label='What was your childhood nickname?'
                    type='text'
                    containerClass='mb-0'
                    onChange={(e) => updateUser('secretAnswer', e.target.value)}
                    style={{marginBottom: '1rem'}}
                  />
                  <p className='font-small grey-text d-flex justify-content-end' onClick={() => changeRecoverState()}> recover password</p>
                  <MDBBtn {...themeProps} type='button' rounded className='btn-block z-depth-1a' onClick={ logWithRecoveredPassword}>
                    Recover 
                  </MDBBtn>
                 </> : <> <MDBInput
                  label='Password'
                  group
                  type='password'
                  validate
                  containerClass='mb-0'
                  value={user.password}
                  onChange={(e) => updateUser('password', e.target.value)}
                  style={{marginBottom: '1rem'}}
                />
                <p className='font-small grey-text d-flex justify-content-end' onClick={() => changeRecoverState()}> recover password</p>
                <MDBBtn {...themeProps} type='button' rounded className='btn-block z-depth-1a' onClick={login}>
                  Sign In
                </MDBBtn> 
                </>
                  }
                </div>
              ) : (
                <div>
                  <MDBInput
                    label='Name'
                    group
                    type='text'
                    validate
                    error='wrong'
                    success='right'
                    value={user.displayName}
                    onChange={(e) => updateUser('displayName', e.target.value)}
                  />
                  <MDBInput
                    label='Email'
                    group
                    type='email'
                    validate
                    error='wrong'
                    success='right'
                    value={user.email}
                    onChange={(e) => updateUser('email', e.target.value)}
                  />

                  <MDBInput
                    label='Password'
                    group
                    type='password'
                    validate
                    containerClass='mb-0'
                    value={user.password}
                    onChange={(e) => updateUser('password', e.target.value)}
                  />
                  <MDBInput
                    label='Confirm password'
                    group
                    type='password'
                    validate
                    containerClass='mb-0'
                    value={user.confirmPassword}
                    onChange={(e) => updateUser('confirmPassword', e.target.value)}
                  />
                   <MDBInput
                    label='What was your childhood nickname?'
                    group
                    type='text'
                    validate
                    containerClass='mb-0'
                    onChange={(e) => updateUser('secretAnswer', e.target.value)}
                  />
                  <div className='text-center mb-3'>
                    <MDBBtn type='button' rounded  {...themeProps} className='btn-block z-depth-1a' onClick={register}>
                      Sign Up
                    </MDBBtn>
                  </div>
                </div>
              )}
            </MDBCardBody>
            <MDBModalFooter className='mx-5 pt-3 mb-1'>
              <p className='font-small grey-text d-flex justify-content-end'>
                {isMember ? 'Not' : 'Already'} a member?
                <MDBBtn
                  {...themeProps}
                  rounded
                  className='py-0 px-3 my-0 ml-3 text-small'
                  onClick={() => {
                    setIsMember(!isMember);
                  }}>
                  Sign {isMember ? 'Up' : 'In'}
                </MDBBtn>
              </p>
            </MDBModalFooter>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default SignIn;
