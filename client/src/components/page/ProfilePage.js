import React, { useState, useContext, useEffect } from 'react';
import AuthContext from '../../providers/AuthContext';
import api from '../../api';
import { MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBCol, MDBIcon, MDBInput, MDBRow, MDBCardFooter } from 'mdbreact';
import ThemeContext from '../../providers/ThemeContext';
import { notify } from '../common/Notification'

const ProfilePage = () => {

  const { loggedUser } = useContext(AuthContext);
  const [user, setUser] = useState({});
  const [, setError] = useState(null);
  const { themeProps } = useContext(ThemeContext);

  const [nameState, setNameState] = useState(true);
  const [passwordState, setPasswordState] = useState(false);
  const [updatedName, setUpdatedName] = useState({
    displayName: ''
  });
  const [updatedPass, setUpdatedPass] = useState({
    password: '',
    confirmPassword: ''
  });
  

  const changeName = () => setNameState(!nameState);
  const changePass = () => setPasswordState(!passwordState);

  const updateName = (prop, value) =>
    setUpdatedName({
      ...updatedName,
      [prop]: value
    });

  const updatePass = (prop, value) =>
    setUpdatedPass({
      ...updatedPass,
      [prop]: value
    });

  useEffect(() => {
      api
        .getUserById(loggedUser.id)
        .then((user) => setUser({ ...user }))
        .catch((error) => setError(error.message));
  }, [loggedUser]);

  const id = user.id


  const updateUserName = (data) => {
    api.updateUserDisplayName(id, data)
      .then((result) => {
        if (result.error) {
          notify(result.message)
        } else {
          notify(`Name changed`)
        }
        setUser((prevUser) => {
          prevUser.displayName = result.displayName
          return { ...prevUser }
        })
        setNameState(!nameState)
      })
      .catch((error) => setError(error.message));
  };

  const newPass = (data) => {
    if(data.password !== data.confirmPassword) {
      notify(`Password doesn't match`)
    }
    api.updateUserPassword(id, data)
      .then((result) => {
        if (result.error) {
          notify(result.message)
        } else {
          notify(`Password changed`)
        }
        setUser((prevUser) => {
          prevUser.password = result.password
          prevUser.confirmPassword = result.confirmPassword
          return { ...prevUser }
        })
        setPasswordState(!passwordState)
      })
      .catch((error) => setError(error.message));
  };
 
  return (
    // <MDBRow className='justify-content-center'>
    //   <MDBCol style={{ textAlign: 'center' }}>
        <MDBCard style={{ textAlign:'center', width: "25rem", margin: '2rem auto' }}>
          <MDBCardImage className="card-img-top p-4" src={user.picture} waves style={{ margin: "auto", borderRadius: '50%' }} />
          <MDBCardBody>
            <MDBCardTitle>{user.displayName} </MDBCardTitle>
            {nameState === true
              ? null
              : (
                <>
                  <input type='text' id='edit-kanban-name' placeholder={`Type new name`}
                    onChange={(e) => updateName('displayName', e.target.value)} />
                  <MDBBtn {...themeProps} size='md' onClick={() => updateUserName(updatedName)}>Change</MDBBtn>
                </>
              )}
            <MDBCardText>
              {user.email}
            </MDBCardText>
            {passwordState ?
            <div className='text-center mb-3'>
            <MDBInput
              label='New Password'
              type='password'
              containerClass='mb-0'
            onChange={(e) => updatePass('password', e.target.value)}
            />
            <MDBInput
              label='Confirm Password'
              type='password'
              containerClass='mb-0'
            onChange={(e) => updatePass('confirmPassword', e.target.value)}
            />
            <MDBBtn {...themeProps} type='button' rounded className='btn-block z-depth-1a' onClick={() => newPass(updatedPass)}>
              Change Now
                </MDBBtn>
          </div>
          : null
            }
          </MDBCardBody>
          <MDBCardFooter>
            <MDBBtn {...themeProps} size='md' onClick={changePass}>{passwordState? "Cancel" : "Change password"}</MDBBtn>
            <MDBBtn {...themeProps} size='md' onClick={changeName}>Change Name</MDBBtn>
            </MDBCardFooter>
        </MDBCard>
        
      // </MDBCol>
      // </MDBRow>
    // </div>
  );
};

export default ProfilePage;
