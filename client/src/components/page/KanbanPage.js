import React, { useContext } from 'react';
import { Switch, Route } from 'react-router-dom';
import { MDBRow, MDBCol, MDBCardImage, MDBCard, MDBCardBody, MDBNavLink } from 'mdbreact';
import AllKanbans from '../kanban/AllKanbans';
import MyKanbans from '../kanban/MyKanbans';
import CreateKanban from '../kanban/CreateKanban';
import ThemeContext from '../../providers/ThemeContext';
import Chat from '../common/Chat';

const KanbanPage = () => {
  const { themeProps } = useContext(ThemeContext);

  return (
    <MDBRow className='m-0 py-3'>
      <MDBCol style={{ minWidth: '300px', maxWidth: '300px' }}>
        <MDBCard style={{ height: 'calc(100vh - 136px)' }} className='m-0'>
          <MDBCardImage className='text-center rounded border-bottom pt-3 pb-2' tag='div'>
            <h4> Kanban Board</h4>
          </MDBCardImage>
          <MDBCardBody cascade className='text-center h-100 p-3'>
            <MDBNavLink {...themeProps} className='btn' to='/kanban/all'>
              ALL KANBANS
            </MDBNavLink>
            <MDBNavLink {...themeProps} className='btn' to='/kanban/my'>
              MY KANBANS
            </MDBNavLink>
            <MDBNavLink {...themeProps} className='btn' to='/kanban/create'>
              CREATE KANBAN
            </MDBNavLink>
          </MDBCardBody>
          <Chat room={'Kanban'} />
        </MDBCard>
      </MDBCol>
      <MDBCol>
        <MDBCard style={{ height: 'calc(100vh - 136px)', overflowY: 'scroll', minWidth: '300px' }} className='m-0'>
          <Switch>
            <Route path='/kanban/all' component={AllKanbans} />
            <Route path='/kanban/my' exact component={MyKanbans} />
            <Route path='/kanban/create' exact component={CreateKanban} />
          </Switch>
        </MDBCard>
      </MDBCol>
    </MDBRow>
  );
};

export default KanbanPage;
