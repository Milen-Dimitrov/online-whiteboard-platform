import React from 'react';
import {
  MDBCarousel,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBView,
  MDBMask,
  MDBContainer
} from "mdbreact";


const WhiteboardPage = () => {
  return (
    <div className='center text-center mt-4 '>
      <h1 className='font-weight-bold text-black-50'>Welcome to Whiteboard</h1>
      <h4 className='mb-4 font-weight-bold text-black-50' >Experience how online whiteboard boosts your creativity, productivity and collaboration! </h4>

      <MDBContainer style={{ width: '45%' }}>
        <MDBCarousel
          activeItem={1}
          length={3}
          showControls={true}
          showIndicators={true}
          className="z-depth-1"
        >
          <MDBCarouselInner >
            <MDBCarouselItem itemId="1">
              <MDBView overlay="d-block w-100 ">
                <img
                  className="d-block w-100"
                  height='360px'
                  src="https://www.mibellegroup.com/blog/wp-content/uploads/2017/03/Design-Thinking_Mibelle-Group-1024x684.jpg"
                  alt="First slide"
                />
                <MDBMask overlay="black-slight" />
              </MDBView>
            </MDBCarouselItem>
            <MDBCarouselItem itemId="2">
              <MDBView>
                <img
                  className="d-block w-100"
                  height='360px'
                  src="https://s3.amazonaws.com/cms.ipressroom.com/108/files/20145/53a0cdb6fe058b3d91023ba7_1430Wi_02/1430Wi_02_7b0ea989-f091-4980-924c-ae11f8a627de-prv.jpg"
                  alt="Second slide"
                />
                <MDBMask overlay="black-slight" />
              </MDBView>
            </MDBCarouselItem>
            <MDBCarouselItem itemId="3">
              <MDBView>
                <img
                  className="d-block w-100"
                  height='360px'
                  src="https://b8g9x2x5.rocketcdn.me/wp-content/uploads/2018/08/team-teamwork-together-ss-1920_b1tngv.jpg"
                  alt="Third slide"
                />
                <MDBMask overlay="black-slight" />
              </MDBView>
            </MDBCarouselItem>
          </MDBCarouselInner>
        </MDBCarousel>
      </MDBContainer>
      <br></br>
      {/* <h4 className='grey-text mt-4'>Technologies used</h4> */}
      <div style={{ display: 'flex', justifyContent: 'center', marginTop: '3.5rem' }}>
        <img
          className='mr-3'
          height='60px'
          width='70rem'
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQT7HZFCI0Euwzq8qotUFeCbHhhISYdqUt4rg&usqp=CAU"
          alt="Third slide"
        />
        <img
          className='mr-3'
          height='60px'
          width='70rem'
          src="https://devclass.com/wp-content/uploads/2019/10/typescript.png"
          alt="Third slide"
        />
        <img
          className='mr-3'
          height='60px'
          width='70rem'
          src="https://mildaintrainings.com/wp-content/uploads/2017/11/react-logo.png"
          alt="Third slide"
        />
        <img
          className='mr-3'
          height='60px'
          width='70rem'
          src="https://cdn.dribbble.com/users/808903/screenshots/3831862/dribbble_szablon__1_1.png"
          alt="Third slide"
        />
        <img
          className='mr-3'
          height='60px'
          width='70rem'
          src="https://cdn-media-1.freecodecamp.org/images/1*DF0g7bNW5e2z9XS9N2lAiw.jpeg"
          alt="Third slide"
        />
      </div>
    </div>
  );
};

export default WhiteboardPage;
