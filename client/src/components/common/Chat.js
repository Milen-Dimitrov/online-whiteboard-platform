import React, { useState, useContext, useEffect } from 'react';
import { MDBCard, MDBCardBody, MDBCardFooter, MDBInput, MDBListGroup, MDBListGroupItem } from 'mdbreact';
import io from 'socket.io-client';
import AuthContext from '../../providers/AuthContext';
import { BASE_URL } from '../../api/index';
const socket = io(BASE_URL);

const Chat = ({ room }) => {
  const [chat, setChat] = useState([]);
  const { loggedUser } = useContext(AuthContext);

  const addMessage = ({ sender, message }) => {
    setChat((prevChat) => {
      return prevChat.concat({ timestamp: Date.now(), date: new Date().toDateString(), sender, message });
    });
  };

  const connect = () => {
    socket.emit('joinRoom', { userId: socket.id, userName: loggedUser.displayName, room });
    socket.on('msgToClient', addMessage);
  };

  useEffect(connect, []);
  const [message, setMessage] = useState('');
  const onMessageChange = (e) => setMessage(e.target.value);

  const onMessageSubmit = (e) => {
    e.preventDefault();
    socket.emit('msgToServer', { userId: socket.id, sender: loggedUser.displayName, message, room });
    setMessage('');
  };

  return (
    <div style={{ position: 'absolute', top: '45%', bottom: 0, left: 0, right: 0 }}>
      <MDBCard className='h-100'>
        <MDBCardBody className='overflow-auto'>
          <MDBListGroup>
            {chat.map(({ timestamp, date, sender, message }) => (
              <MDBListGroupItem className='rounded mb-3' key={timestamp} color="success" >
                <h6 className='border-bottom font-weight-bold w-100'>
                  {sender} <time className='font-small'>({date})</time>
                </h6>

                {message}
              </MDBListGroupItem>
            ))}
          </MDBListGroup>
        </MDBCardBody>
        <MDBCardFooter>
          <form onSubmit={onMessageSubmit}>
            <MDBInput
              icon='paper-plane'
              size='sm'
              name='message'
              type='text'
              onChange={onMessageChange}
              value={message}
              id='msg'
              variant='outlined'
              label='Message'
            />
          </form>
        </MDBCardFooter>
      </MDBCard>
    </div>
  );
};

export default Chat;
