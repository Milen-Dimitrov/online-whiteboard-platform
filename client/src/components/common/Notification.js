import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

toast.configure()

export const notify = (message) => {
    toast(`${message}`, { 
      position: toast.POSITION.TOP_CENTER,
      autoClose: 3000
    })
  }

  toast.configure()