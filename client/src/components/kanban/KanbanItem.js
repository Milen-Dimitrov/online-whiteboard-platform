import React, {
  useContext,
  useState,
} from 'react';
import propTypes from 'prop-types';
import { withRouter, useLocation } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import ThemeContext from '../../providers/ThemeContext';
import { MDBCard, MDBCardBody, MDBCardImage, MDBCardText, MDBLink, MDBIcon, MDBBtn, MDBInput, MDBTooltip, MDBCardFooter, MDBCardHeader } from 'mdbreact';
import api from '../../api';
import { notify } from '../common/Notification'

const KanbanItem = ({ kanban, currentKanban, setCurrentKanban, deleteKanban, updateKanban, updateAccess }) => {

  const location = useLocation();
  const isAllKanban = location.pathname.includes('all');
  const { loggedUser } = useContext(AuthContext);
  const { themeProps } = useContext(ThemeContext);

  const [updatedName, setUpdatedName] = useState({
    name: ''
  });
  const [inviteState, setInviteState] = useState(true);
  const [updatedInvite, setUpdatedInvite] = useState({
    invitationEmail: ''
  });
  const id = kanban.id;

  const [privacyState, setPrivacyState] = useState(true);
  const [updatedPrivacy, setUpdatedPrivacy] = useState({
    isPublic: true
  });
  const [, setError] = useState(null);


  const changePrivacy = () => setPrivacyState(!privacyState);

  const updateKanbanPrivacy = (prop, value) => {
    setUpdatedPrivacy({
      [prop]: value
    })
  };


  const updateKanbanName = (prop, value) =>
    setUpdatedName({
      ...updatedName,
      [prop]: value
    });

  const inviteSomeone = () => api.invite(updatedInvite, id)
    .then((result) => {
      notify(`${result.message}`);
      if (result.error) {
        notify(result.message)
      }
    })
    .catch((error) => setError(error.message))
    .then(setInviteState(!inviteState))

  const changeInviteState = () => {
    setInviteState(!inviteState);
  };

  const updateInviteName = (prop, value) =>
    setUpdatedInvite({
      ...updatedInvite,
      [prop]: value
    });

  return (
    <MDBCard style={{ width: '16rem', margin: '0.5rem' }}>
      <MDBCardHeader>
        {loggedUser.id === kanban.kanbanCreator.id && !isAllKanban ? (
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <MDBTooltip domElement tag='span' placement='top' >
              <span onClick={() => changeInviteState()}>
                <MDBIcon icon='user-plus' onClick={() => changeInviteState()} />
              </span>
              <div>Invite</div>
            </MDBTooltip>
            <div style={{ position: 'relative' }}>
              <MDBTooltip domElement tag='span' placement='top'>
                <span onClick={() => deleteKanban(id)}>
                  <MDBIcon far icon='trash-alt' />
                </span>
                <div>Delete</div>
              </MDBTooltip>
            </div>
          </div>
        )
        : null
      }


        <MDBCardImage className='img-fluid' src={kanban.kanbanCreator.picture} style={{ borderRadius: '50%' }} waves />

        {inviteState ? (

          <div style={{ position: 'relative' }}>
            <h6 className='font-weight-bold my-2 text-center'>
              {kanban.name}
              <MDBTooltip domElement tag='span' placement='right'>
                <span onClick={() => setCurrentKanban(kanban.id !== currentKanban?.id ? kanban : {})}>
                  <MDBIcon far icon='edit' className='ml-1' />
                </span>
                <div>Edit name</div>
              </MDBTooltip>
            </h6>
          </div>
        ) : (
            <>
              <MDBInput hint="Your e-mail" type="email"
                onChange={(e) => updateInviteName('invitationEmail', e.target.value)} >
              </MDBInput>
              <MDBBtn  {...themeProps} onClick={inviteSomeone}>Invite</MDBBtn>
            </>
          )}
      </MDBCardHeader>
      <MDBCardBody className='p-3'>

        {kanban.id !== currentKanban?.id
          ? null
          : (
            <>
              <input type='text'
                id='edit-kanban-name'
                placeholder={`Type new name`}
                onChange={(e) => updateKanbanName('name', e.target.value)}
              />
              <MDBBtn {...themeProps} onClick={() => updateKanban(id, updatedName)}>Submit</MDBBtn>
            </>
          )}
        <MDBCardText>Kanban creator: {kanban.kanbanCreator.displayName}</MDBCardText>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <MDBCardText>Kanban status: {kanban.isPublic ? `Public` : `Private`}
            <MDBTooltip domElement tag='span' placement='top' >
              <span onClick={changePrivacy}>
                <MDBIcon icon='sync' hover={`switch status`} className='ml-2' />
              </span>
              <div>Change status</div>
            </MDBTooltip>
          </MDBCardText>
        </div>
        {privacyState
          ? null
          :
          <div className='custom-control custom-switch'>
            <select className='browser-default custom-select' onChange={(e) => updateKanbanPrivacy('isPublic', e.target.value)}>
              <option value={true}>Public</option>
              <option value={false}>Private</option>
            </select>
            <MDBBtn {...themeProps} onClick={() => updateAccess(id, updatedPrivacy)} >Update</MDBBtn>
          </div>
        }

      </MDBCardBody>
      <MDBCardFooter>
        <MDBLink {...themeProps} to={`all/${id}`} className='btn rounded w-100 p-2 mx-0'>
          Show
        </MDBLink>
      </MDBCardFooter>
    </MDBCard>
  );
};

KanbanItem.propTypes = {
  kanban: propTypes.object.isRequired
};

export default withRouter(KanbanItem);
