import React, { useState } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import api from '../../api';
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBInput, MDBBtn } from 'mdbreact';
import { notify } from '../common/Notification';

const CreateKanban = () => {
  const history = useHistory();
  const [kanban, setKanban] = useState({ name: '', isPublic: true });

  const onKanbanChange = (key) => ({ target: {value} }) => {
    setKanban({ ...kanban, [key]: value });
  };

  const addKanban = () => {
    if(kanban.isPublic === 'false'){
      kanban.isPublic = false
    }
    api.createKanban(kanban)
    .then(history.push(`/kanban/my`));
    notify(`Kanban created!`)
  };

  return (
    <MDBContainer>
      <MDBRow>
        <MDBCol>
          <MDBCard style={{ margin: '5rem auto', width: '520px' }}>
            <MDBCardBody className='mx-4'>
              <div className='text-center'>
                <h3 className='dark-grey-text mb-5'>
                  <strong>Create New Kanban</strong>
                </h3>
              </div>

              <MDBContainer className='mt-5 p-0'>
                <select className='browser-default custom-select' onChange={onKanbanChange('isPublic')}>
                  <option value={true}>Public</option>
                  <option value={false}>Private</option>
                </select>
                <MDBInput
                  labelClass='mx-2'
                  label='Name'
                  group
                  type='text'
                  validate
                  containerClass='mb-0'
                  value={kanban.name}
                  onChange={onKanbanChange('name')}
                />

                <MDBBtn color='primary' rounded className='btn-primary btn-block z-depth-1a' onClick={addKanban}>
                  Create
                </MDBBtn>
              </MDBContainer>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default withRouter(CreateKanban);
