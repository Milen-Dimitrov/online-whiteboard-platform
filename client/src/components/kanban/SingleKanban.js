import React, { useEffect, useState } from 'react';
import { withRouter, useParams, useHistory } from 'react-router-dom';
import Board, { onDataChange } from 'react-trello';
import api from '../../api';
import { notify } from '../common/Notification';

const SingleKanban = () => {
  const { id } = useParams();
  const history = useHistory();
  const [shouldUpdate, setShouldUpdate] = useState(true);
  const [currentKanban, setCurrentKanban] = useState({ lanes: [] });

  useEffect(() => {
    fetch(`http://localhost:3000/kanbans/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        setShouldUpdate(false)
        if (result.error) {
          notify(`${result.message}`)
          history.push(`/kanban/all`)
        } else {
          setCurrentKanban(result)
        }})
        .catch((error) => {
        console.error(error.message)
      })
  }, [id, shouldUpdate, history])

  const update = () => setShouldUpdate(true);

  const onLaneAdd = (data) => api.createLane({ ...data, kanbanId: id }).then(update);
  const onLaneUpdate = (laneId, data) => api.updateLane(laneId, data).then(update);
  const onLaneDelete = (laneId) => api.deleteLane(laneId).then(update);
  const onCardAdd = ({ title, description, label }, laneId) => api.createCard(laneId, { title, description, label }).then(update);
  const onCardDelete = (cardId) => api.deleteCard(cardId).then(update);
  const onCardMoveAcrossLanes = (fromLaneId, toLaneId, cardId, index) =>
    api.updateCardPosition({ toLaneId: toLaneId }, cardId).then(console.log);

  return (
    <div className='Kanban' style={{ position: 'absolute', bottom: 0, left: 0, right: 0, top: 0 }}>
      <h4 className='m-3 text-primary-dark'>{currentKanban.name}</h4>
      <Board
        title='test'
        data={currentKanban}
        draggable
        id='EditableBoard1'
        onDataChange={onDataChange}
        onLaneAdd={onLaneAdd}
        onLaneUpdate={onLaneUpdate}
        onLaneDelete={onLaneDelete}
        onCardAdd={onCardAdd}
        onCardDelete={onCardDelete}
        onCardMoveAcrossLanes={onCardMoveAcrossLanes}
        editable
        canAddLanes
        editLaneTitle
      />
    </div>
  );
};

export default withRouter(SingleKanban);
