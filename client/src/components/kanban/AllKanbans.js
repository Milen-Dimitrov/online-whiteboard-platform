import React, { useState, useEffect, useContext } from 'react';
import { Switch, Route } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import KanbanItem from './KanbanItem';
import api from '../../api';
import SingleKanban from './SingleKanban';
import { notify } from '../common/Notification'


const AllKanbans = () => {
  const [kanbans, setKanbans] = useState([]);
  const [, setError] = useState(null);
  const [currentKanban, setCurrentKanban] = useState({});
  const { loggedUser } = useContext(AuthContext);
  const [privacyState, setPrivacyState] = useState(true);

  const changePrivacy = () => setPrivacyState(!privacyState);



  useEffect(() => {
    api
      .getAllPublicKanbans()
      .then((kanbans) => {
        setKanbans(kanbans);
      })
      .catch((error) => setError(error.message));
    return window.removeEventListener('', api.getAllPublicKanbans);
  }, [privacyState]);

  const deleteKanban = id => {

    api
      .kanbanDelete(id)
      .catch((error) => setError(error.message));
      notify(`Kanban deleted!`)

    const newKanbans = kanbans.filter((k) => k.id !== id);
    setKanbans(newKanbans);
  };

  const updateKanban = (id, data) => {
    api.kanbanUpdate(id, data)
      .then((result) => {

        if (result.error) {
          notify(result.message)
        }
        
        setCurrentKanban({...result})
        setKanbans(kanbans.map(kanban => kanban.id === id ? {...kanban, ...result}: kanban))
        notify(`Name changed`)


      })
      .catch((error) => setError(error.message));
  };

  const updateAccess = (id, data) => {
    api.kanbanUpdate(id, data)
      .then((result) => {
        console.log(result);
        if (result.error) {
          notify(result.message)
        }
        setKanbans(kanbans.map(kanban => kanban.id === id ? {...kanban, ...result}: kanban))
        changePrivacy()
        notify(`Status changed`)
      })
      .catch((error) => setError(error.message));
  };

  return (
    <div>
    <div className='d-flex' style={{ padding: '0.5rem' }}>
      <Switch>
        <Route
          path='/kanban/all'
          exact
          render={() =>
            kanbans?.map((k) => (
              <KanbanItem
                key={k.id}
                kanbanKey={k.id}
                name={k.name}
                kanbanCreator={k.kanbanCreator}
                deleteKanban={deleteKanban}
                loggedUser={loggedUser}
                updateKanban={updateKanban}
                currentKanban={currentKanban}
                setCurrentKanban={setCurrentKanban}
                updateAccess={updateAccess}
                kanban={k}
              />
            ))
          }
        />
        <Route path='/kanban/all/:id' exact component={SingleKanban} />
      </Switch>
    </div>
    </div>
  );
};

export default AllKanbans;
