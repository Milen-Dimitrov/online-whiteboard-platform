import React, { useState, useEffect, useContext } from 'react';
import { MDBNav, MDBNavItem, MDBBtn } from 'mdbreact';
import AuthContext from '../../providers/AuthContext';
import SingleKanban from './KanbanItem';
import api from '../../api';
import { notify } from '../common/Notification';
import { withRouter } from 'react-router-dom';

const MyKanbans = () => {
  const [privateKanbans, setPrivateKanbans] = useState([]);
  const [publicKanbans, setPublicKanbans] = useState([]);
  const [currentKanban, setCurrentKanban] = useState({});
  const { loggedUser } = useContext(AuthContext);

  const [showPrivate, setShowPrivate] = useState(false);
  const [, setError] = useState(null);
  const [privacyState, setPrivacyState] = useState(true);

  const changePrivacy = () => setPrivacyState(!privacyState);


  useEffect(() => {
    api.getPrivateKanbans().then(setPrivateKanbans);
    return window.removeEventListener('', api.getPrivateKanbans);
  }, [privacyState]);

  useEffect(() => {
    api.getPublicKanbans().then(setPublicKanbans);
    return window.removeEventListener('', api.getPublicKanbans);
  }, [privacyState]);

  const kanbanToDelete = (id) => {
    if (showPrivate) {
      const foundPrivate = privateKanbans.find((k) => k.id === id);

      if (foundPrivate.kanbanCreator.id !== loggedUser.id) {
        return notify(`You don't have the rights to delete the kanban!`);
      }
      api.kanbanDelete(id).then(notify(`You have just deleted this kanban!`));
      const newKanbans = privateKanbans.filter((k) => k.id !== id);
      setPrivateKanbans(newKanbans);
    } else {
      const foundPublic = publicKanbans.find((k) => k.id === id);
      if (!foundPublic) {
        return notify(`No kanban with id ${id} found!`);
      }

      if (foundPublic.kanbanCreator.id !== loggedUser.id) {
        return notify(`You don't have the rights to delete the kanban!`);
      }
      api.kanbanDelete(id).then(notify(`You have just deleted this kanban!`));
      const newKanbans = publicKanbans.filter((k) => k.id !== id);
      setPublicKanbans(newKanbans);
    }
  };

  const updateKanban = (id, data) => {
    api.kanbanUpdate(id, data)
      .then((result) => {

        if (result.error) {
          notify(result.message)
        }

        console.log(result);
        setCurrentKanban({...result})
        setPrivateKanbans(privateKanbans.map(kanban => kanban.id === id ? {...kanban, ...result}: kanban))
        setPublicKanbans(publicKanbans.map(kanban => kanban.id === id ? {...kanban, ...result}: kanban))

        notify(`Name changed`)

      })
      .catch((error) => setError(error.message));
  };

  const updateAccess = (id, data) => {
    api.kanbanUpdate(id, data)
      .then((result) => {
        console.log(result);
        if (result.error) {
          notify(result.message)
        }
        setPrivateKanbans(privateKanbans.map(kanban => kanban.id === id ? {...kanban, ...result}: kanban))
        setPublicKanbans(publicKanbans.map(kanban => kanban.id === id ? {...kanban, ...result}: kanban))
        changePrivacy()
        notify(`Status changed`)
      })
      .catch((error) => setError(error.message));
  };

  return (
    <div>
      <MDBNav className='mb-2' tabs>
        <MDBNavItem>
          <MDBBtn size='sm' color='primary' active={!showPrivate} onClick={() => setShowPrivate(false)} role='tab'>
            Public
          </MDBBtn>
        </MDBNavItem>
        <MDBNavItem>
          <MDBBtn size='sm' color='primary' active={showPrivate} onClick={() => setShowPrivate(true)} role='tab'>
            Private
          </MDBBtn>
        </MDBNavItem>
      </MDBNav>
      <div style={{ display: "flex", flexFlow: "row wrap" }}>
      {showPrivate
        ? privateKanbans?.map((k) => (
          <SingleKanban
            key={k.id}
            kanbanKey={k.id}
            name={k.name}
            kanbanCreator={k.kanbanCreator}
            deleteKanban={kanbanToDelete}
            loggedUser={loggedUser}
            updateKanban={updateKanban}
            currentKanban={currentKanban}
            setCurrentKanban={setCurrentKanban}
            updateAccess={updateAccess}
            kanban={k}
          />
        ))
        : publicKanbans?.map((k) => (
          <SingleKanban
            key={k.id}
            kanbanKey={k.id}
            name={k.name}
            kanbanCreator={k.kanbanCreator}
            deleteKanban={kanbanToDelete}
            loggedUser={loggedUser}
            updateKanban={updateKanban}
            currentKanban={currentKanban}
            setCurrentKanban={setCurrentKanban}
            updateAccess={updateAccess}
            kanban={k}
          />
        ))}
        </div>
    </div>
  );
};

export default withRouter(MyKanbans);
