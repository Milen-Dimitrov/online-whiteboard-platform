import { createContext } from 'react';

const darkStyles = { color: 'primary-color-dark', style: { backgroundColor: '#0d47a1', color: '#EFEFEF' } };
const lightStyles = { color: 'primary-color', style: { backgroundColor: '#4285F4', color: '#EFEFEF' } };

export const getThemeProps = (dark) => (dark ? darkStyles : lightStyles);

const ThemeContext = createContext({
  themeProps: {}
});

export default ThemeContext;
